1.0.0 (2021-06-21)
 * Primera versión.
 * Creación del proyecto.
 * Añadida lógica de toda la API.
 * Creación de integración con servicios externos.
 * Creación de pruebas unitarias y de integración.
