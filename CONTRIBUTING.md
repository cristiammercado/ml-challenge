
## Contributing

1. [Realizar un fork](https://gitlab.com/cristiammercado/ml-challenge).
2. Crea tu rama de feature (`git checkout -b feature/any-feature`).
3. Realiza el commit de tus cambios (`git commit -am 'Añadida nuevo feature'`).
4. Realiza push de tu rama al servidor (`git push origin feature/any-feature`).
5. Crea un nuevo [MR](https://gitlab.com/cristiammercado/ml-challenge/-/merge_requests/new).
