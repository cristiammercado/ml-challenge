FROM openjdk:11-jre-slim

COPY target/ml-api.jar /ml-api.jar

EXPOSE 8080
ENTRYPOINT ["java", "-Dlogback.configurationFile=logback.xml", "-jar", "ml-api.jar", "--env=prod"]
