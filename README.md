# ML API (Challenge)

API para obtener información de una IP como país de ubicación, moneda local y tasas de cambios, además de administrar una lista negra de IPs. Esta API hace parte de un [challenge](https://gitlab.com/cristiammercado/ml-challenge/-/blob/master/extras/challenge_lyr.pdf) para MercadoLibre.

Esta API ha sido construída en [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) con [Vertx](https://vertx.io/), haciendo uso del concepto de [programación reactiva](http://frangarcia.net:8484/). También se usó como base de datos MySQL. Se crearon varias integraciones con diferentes APIs para obtener partes de la información asociada a la IP. Las plataformas son [IPApi](https://ipapi.co/), [RestCountries](http://restcountries.eu/) y [Fixer](https://fixer.io/), en algunas se creó una cuenta gratuita para efectos de pruebas.

- **Versión actual:** 1.0.2
- **Autor:** Cristiam Mercado Jiménez - <cristiammercado@hotmail.com>

#### **URLs** ####

- **Repositorio:** [https://gitlab.com/cristiammercado/ml-challenge](https://gitlab.com/cristiammercado/ml-challenge)
- **IPApi:** [https://ipapi.co/](https://ipapi.co/)
- **RestCountries:** [http://restcountries.eu/](http://restcountries.eu/)
- **Fixer:** [https://fixer.io/](https://fixer.io/)

----------
#### **REQUERIMIENTOS MÍNIMOS** ####

- Git ([Windows](https://git-scm.com/download/win)) ([Linux](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04))
- Java JDK 11.0.10 ([Windows](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)) ([Linux](https://docs.oracle.com/en/java/javase/11/install/installation-jdk-linux-platforms.html#GUID-737A84E4-2EFF-4D38-8E60-3E29D1B884B8))
- Apache Maven 3.8.1 ([Windows](https://maven.apache.org/download.cgi)) ([Linux](https://sdkman.io/sdks#maven))
- MySQL 8.0.25 ([Windows](https://dev.mysql.com/doc/refman/8.0/en/windows-installation.html)) ([Linux](https://dev.mysql.com/doc/refman/8.0/en/linux-installation.html))

No olvide configurar las variables del sistema:

- `JAVA_HOME`: apuntando a la carpeta de instalación del JDK.
- `MAVEN_HOME` o `M2_HOME`:  apuntando a la carpeta de instalación de Maven.
- Agregar en el path del sistema operativo la carpeta `bin` de la instalación de Maven.

#### **CONFIGURACIÓN INICIAL** ####

El código fuente está en un proyecto tipo Maven que se puede abrir con cualquier IDE que admita este tipo de proyecto (se recomienda *IntelliJ IDEA*). Se recomienda que localmente no tenga ninguna aplicación ejecutándose en el puerto 8080.

Para comenzar a codificar, recuerde ejecutar el comando `mvn clean compile` en una consola ubicada en la carpeta raíz del proyecto. Este comando descargará todas las dependencias necesarias para iniciar el proyecto y realizará la generación de código en tiempo de compilación (Immutables).

Para comenzar a codificar, recuerde que se usa [GitFlow](http://nvie.com/posts/a-successful-git-branching-model/) (al menos para las ramas `feature/**`, `develop` y `master`) para manejar las ramas de `Git`, así que asegúrese de que esté ubicado en su rama `feature/**` antes de realizar cualquier cambio.

Considere instalar [Apache Maven](https://maven.apache.org/) globalmente en su sistema operativo (también puede usar el wrapper). Para saber si se instaló correctamente a nivel global, abra una consola de comandos y ejecute:

```
mvn -v
```

Esto debería imprimir algo como (ejemplo in `Windows`):

```
Apache Maven 3.8.1 (05c21c65bdfed0f71a2f2ada8b84da59348c4c5d)
Maven home: C:\Program Files\Maven\bin\..
Java version: 11.0.10, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk-11.0.10
Default locale: es_CO, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

##### **Base de datos** #####

Una vez realizada la instalación del servidor para el motor de base de datos MySQL, cree manualmente la bases de datos con las siguiente sentencia:

```
CREATE DATABASE IF NOT EXISTS `ml_challenge`;
```

Tenga en cuenta las credenciales de acceso para ingresar al servidor MySQL, donde la aplicación asume que el usuario es `root` y la clave es `root` en el ambiente de desarrollo. Para ambiente de pruebas y productivo debe rescribirse las propiedades acorde a [Config](https://github.com/lightbend/config) en los archivos de configuración de cada ambiente en la carpeta `src/main/resources`.


##### **Migraciones** #####

Las migraciones de las bases de datos las puede encontrar en la carpeta del proyecto `/src/main/resources/migrations`. Si desea crear nuevas migraciones para las bases de datos, debe seguir estas normas:

- El primer caracter del archivo creado debe ser la letra `v`.
- Los siguientes 8 números son la fecha expresada en `añomesdía`. Tenga en cuenta que este número debe ser único con respecto al resto de archivos.
- El siguiente caracter es un doble guión piso.
- El resto es un nombre identificativo de los cambios que se realizan, terminando el archivo con la extensión `.sql`.

Un ejemplo para el nombre de una migración sería:

```
v20210617__create_main_tables.sql
```

La ejecución de las migraciones se realiza automáticamente al iniciar la aplicación. No hace falta ejecutar una acción extra. Para más información acerca de **Flyway**, puede seguir este [enlace](https://flywaydb.org/).

#### **EJECUCIÓN** ####

Para iniciar la aplicación en local, ejecute el comando de Maven:

```
mvn compile exec:exec
```

Dicha ejecución está configurada con [Re-deploy](https://vertx.io/docs/vertx-core/java/#_live_redeploy) de Vertx, por ende si hace algún cambio en el código fuente, automáticamente se reinicia la aplicación con los cambios realizados, lo cual facilita y acelera el desarrollo.

Para ejecutar la aplicación desde un archivo jar, se genera el [FatJar](https://stackoverflow.com/questions/19150811/what-is-a-fat-jar), cuya tarea se puede a través de una consola de comandos con Maven ejecutando el comando:

```
mvn clean compile package
```

Este comando descargará todas las dependencias que usa el proyecto y generará el FatJar en la ruta del proyecto `target/ml-api.jar`. Ejecute este archivo con el comando para ejecutar normalmente `.jar` con Java, esto es:

```
java -jar target/ml-api.jar
```

Con esto, automáticamente verá reflejado en su consola de comandos el arranque de la aplicación y si existe un error al momento de inicializar.

#### **PRUEBAS** ####

Existen pruebas unitarias para diferentes funcionalidades y utilidades de la applicación y para las pruebas de integración se usa la base de datos H2. Para estas pruebas se hace uso de [Vertx JUnit 5](https://vertx.io/docs/vertx-junit5/java/). 

Se usa [JaCoCo](https://www.eclemma.org/jacoco/) para generar un reporte de cobertura de pruebas de la API. 

Puede ejecutar las pruebas usando el comando:

```
mvn clean compile test
```

Verá en la salida de la consola la ejecución de las pruebas y los resultados totales. En ocasiones puede ver salida de errores pero es normal, hace parte de pruebas con flujos excepcionales.

En la carpeta `target/site/jacoco/` encuentra el archivo `index.html` que lo puede abrir en el navegador de su preferencia y ver los resultados de la cobertura.

#### **COMPILACIÓN** ####

Para compilar el proyecto y generar el archivo `jar` ejecutable, solo necesita ejecutar el comando:

```
mvn clean compile package
```

Se generará un archivo `.jar` en la carpeta `target` con el nombre `ml-api.jar`. Este archivo ya contiene todas las dependencias dentro del jar, de tal manera que no debe transportar ni configurar una ruta de dependencias adicional para su ejecución.

#### **DOCUMENTACIÓN** ####

En el código fuente podrá encontrar las clases y métodos comentados.

#### **CURL / POSTMAN** ####

En la carpeta `extras` puede encontrar el archivo de colección `ML Challenge.postman_collection.json` para usar la API importándola en [Postman](https://www.postman.com/). De acuerdo a los requerimientos del challenge, no tiene ningún tipo de autenticación.

También puede ejecutar los siguientes comandos `curl` desde la consola:

- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/status'`
- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/ip'` (Toma la IP del origen del request)
- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/ip/127.0.0.1'`
- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/blacklist?page=1&pagesize=10'`
- `curl -s -L -m 15 -X POST 'http://127.0.0.1:8080/blacklist' -H 'Content-Type: application/json' --data-raw '{"ip": "186.154.185.250"}'` 
- `curl -s -L -m 15 -X DELETE 'http://127.0.0.1:8080/blacklist/127.0.0.1'`

#### **DOCKER** ####

Puede hacer uso de [docker](https://www.docker.com/) para la creación de un contenedor para la ejecución de la aplicación y de la base de datos. Previamente debe tener instalado docker en la máquina y también haber compilado el proyecto.

Para iniciar ejecute el comando:

```
docker compose up
```

Este comando creará los servicios descritos en el archivo `docker-composer.yml`: un servicio de MySQL versión 8 y la aplicación descrita en el archivo `Dockerfile`.

Puede que la primera vez al inicio la API genere errores y se reinicie, esto es mientras el contenedor de MySQL arranca y realiza las configuraciones iniciales, de acuerdo al comportamiento de Docker.

#### **AYUDA / SOPORTE** ####

Si tiene algún problema, puede enviarme un correo electrónico a [cristiammercado@hotmail.com](mailto:cristiammercado@hotmail.com) o puede utilizar el formulario de contacto en [mi página](https://cristiammercado.com/#contact).

Para informes de bugs, por favor [abra un issue en GitLab](https://gitlab.com/cristiammercado/ml-challenge/-/issues/new).
