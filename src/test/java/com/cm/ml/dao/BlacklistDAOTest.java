package com.cm.ml.dao;

import com.cm.ml.infrastructure.database.DatabaseConnection;
import com.cm.ml.infrastructure.utils.QueryData;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

class BlacklistDAOTest {

    @Test
    void listByPage() {
        assertThrows(Exception.class, () -> {
            BlacklistDAO dao = new BlacklistDAO(null);
            dao.listByPage(new QueryData(1, 10));
        });
    }

    @Test
    void create() {
        assertThrows(Exception.class, () -> {
            DatabaseConnection connection = Mockito.mock(DatabaseConnection.class, RETURNS_DEEP_STUBS);
            when(connection.get()).thenThrow(new SQLException("TEST Exception"));

            BlacklistDAO dao = new BlacklistDAO(connection);
            dao.create("127.0.0.1");
        });
    }

    @Test
    void delete() {
        assertThrows(Exception.class, () -> {
            DatabaseConnection connection = Mockito.mock(DatabaseConnection.class, RETURNS_DEEP_STUBS);
            when(connection.get()).thenThrow(new SQLException("TEST Exception"));

            BlacklistDAO dao = new BlacklistDAO(connection);
            dao.delete("127.0.0.1");
        });
    }

    @Test
    void isIPBlacklisted() {
        assertThrows(Exception.class, () -> {
            BlacklistDAO dao = new BlacklistDAO(null);
            dao.isIPBlacklisted("127.0.0.1");
        });
    }
}
