package com.cm.ml;

import com.cm.ml.infrastructure.config.APIConfig;
import com.cm.ml.infrastructure.config.DeploymentConfig;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.VertxOptions;
import io.vertx.rxjava3.ext.web.RoutingContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

class APILauncherTest {

    @Test
    @DisplayName("Call main method without errors")
    void main1() {
        assertDoesNotThrow(() -> APILauncher.main(new String[]{"--version"}));
    }

    @Test
    @DisplayName("Call main method with an empty parameter")
    void main2() {
        assertDoesNotThrow(() -> APILauncher.main(new String[]{"--version", ""}));
    }

    @Test
    @DisplayName("Call main method with an env parameter")
    void main3() {
        assertDoesNotThrow(() -> APILauncher.main(new String[]{"--version", "--env=test"}));
    }

    @Test
    @DisplayName("Call beforeStartingVertx method without errors")
    void beforeStartingVertx() {
        assertDoesNotThrow(() -> {
            APILauncher al = new APILauncher();
            al.beforeStartingVertx(new VertxOptions());
        });
    }

    @Test
    @DisplayName("Call beforeDeployingVerticle method without errors")
    void beforeDeployingVerticle() {
        assertDoesNotThrow(() -> {
            APILauncher al = new APILauncher();

            Injector injector = Guice.createInjector(new APIConfig());
            injector.injectMembers(al);

            al.beforeDeployingVerticle(new DeploymentOptions());
        });
    }

    @Test
    @DisplayName("Call getMainVerticle method without errors")
    void getMainVerticle() {
        assertDoesNotThrow(() -> {
            APILauncher al = new APILauncher();
            String mainVerticle = al.getMainVerticle();

            assertNotNull(mainVerticle);
        });
    }
}
