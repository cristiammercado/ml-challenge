package com.cm.ml.integration;

import com.cm.ml.MainVerticle;
import com.cm.ml.infrastructure.config.EnvConfig;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.RequestOptions;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(VertxExtension.class)
class IPIntegrationTest {

    public IPIntegrationTest(Vertx v, VertxTestContext vtc) {
        EnvConfig.get().set("test");
        v.deployVerticle(new MainVerticle(), vtc.succeedingThenComplete());
    }

    @Test
    void ipByParameter1(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        client.request(HttpMethod.GET, 8080, "127.0.0.1", "/ip/43.250.192.0")
            .compose(req -> req.send().compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), true);
                assertNotNull(buffer.toJsonObject().getJsonObject("data"));
                vtc.completeNow();
            })));
    }

    @Test
    void ipByParameter2(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        client.request(HttpMethod.GET, 8080, "127.0.0.1", "/ip/186.154.185.")
            .compose(req -> req.send().compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), false);
                assertNotNull(buffer.toJsonObject().getJsonObject("error"));
                vtc.completeNow();
            })));
    }

    @Test
    void ipByParameter3(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        client.request(HttpMethod.POST, 8080, "127.0.0.1", "/blacklist")
            .compose(req -> req.send("{\"ip\":\"186.154.185.250\"}").compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), true);

                client.request(HttpMethod.GET, 8080, "127.0.0.1", "/ip/186.154.185.250")
                    .compose(req -> req.send().compose(HttpClientResponse::body))
                    .onComplete(vtc.succeeding(buffer2 -> vtc.verify(() -> {
                        assertEquals(buffer2.toJsonObject().getBoolean("success"), false);
                        assertNotNull(buffer2.toJsonObject().getJsonObject("error"));
                        vtc.completeNow();
                    })));
            })));
    }

    @Test
    void ipByRequest1(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        client.request(HttpMethod.GET, 8080, "127.0.0.1", "/ip")
            .compose(req -> req.send().compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), false);
                assertNotNull(buffer.toJsonObject().getJsonObject("error"));
                vtc.completeNow();
            })));
    }

    @Test
    void ipByRequest2(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.setMethod(HttpMethod.GET);
        requestOptions.setAbsoluteURI("http://127.0.0.1:8080/ip");
        requestOptions.addHeader("x-forwarded-for", "98.21.203.252");

        client.request(requestOptions)
            .compose(req -> req.send().compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), true);
                assertNotNull(buffer.toJsonObject().getJsonObject("data"));
                vtc.completeNow();
            })));
    }

}
