package com.cm.ml.integration;

import com.cm.ml.MainVerticle;
import com.cm.ml.infrastructure.config.EnvConfig;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(VertxExtension.class)
class UpIntegrationTest {

    public UpIntegrationTest(Vertx v, VertxTestContext vtc) {
        EnvConfig.get().set("test");
        v.deployVerticle(new MainVerticle(), vtc.succeedingThenComplete());
    }

    @Test
    void status(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        client.request(HttpMethod.GET, 8080, "127.0.0.1", "/status")
            .compose(req -> req.send().compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), true);
                assertNotNull(buffer.toJsonObject().getJsonObject("data"));
                vtc.completeNow();
            })));
    }

    @Test
    void notFound(Vertx v, VertxTestContext vtc) {
        HttpClient client = v.createHttpClient();

        client.request(HttpMethod.GET, 8080, "127.0.0.1", "/sfsafsdfas")
            .compose(req -> req.send().compose(HttpClientResponse::body))
            .onComplete(vtc.succeeding(buffer -> vtc.verify(() -> {
                assertEquals(buffer.toJsonObject().getBoolean("success"), false);
                assertNotNull(buffer.toJsonObject().getJsonObject("error"));
                vtc.completeNow();
            })));
    }

}
