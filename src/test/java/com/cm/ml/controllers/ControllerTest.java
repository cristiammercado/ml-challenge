package com.cm.ml.controllers;

import io.vertx.rxjava3.ext.web.RoutingContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ControllerTest extends Controller {

    @Test
    @DisplayName("Build exception response with a Throwable")
    void buildExceptionResponse() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);

        this.buildExceptionResponse(ctx, new Throwable("TEST Error"));
        verify(ctx, times(1)).response();
    }

    @Test
    @DisplayName("Build response with a null ErrorResponse")
    void buildResponse() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);

        this.buildResponse(ctx, 200, null, null);
        verify(ctx, times(1)).response();
    }

    @Test
    @DisplayName("Handle failure with a throwable")
    void handleFailure() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);

        this.handleFailure(ctx, new Throwable("TEST Error"));
        verify(ctx, times(1)).response();
    }

}
