package com.cm.ml.infrastructure.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonUtilsTest extends JsonUtils {

    @Test
    @DisplayName("JSON encode generates exception")
    void encode() {
        String encode = JsonUtils.encode(this);
        assertEquals(encode, "");
    }

}
