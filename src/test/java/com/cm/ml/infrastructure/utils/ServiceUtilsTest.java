package com.cm.ml.infrastructure.utils;

import com.cm.ml.infrastructure.exceptions.BusinessException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ServiceUtilsTest extends ServiceUtils {

    @Test
    @DisplayName("Request with HTTP code different from 200")
    void makeGETRequest1() {
        assertThrows(BusinessException.class, () -> {
            URI uri = new URI("https://www.example.com/dklsxhgfdsbhsdk");
            ServiceUtils.makeGETRequest(uri);
        });
    }

    @Test
    @DisplayName("Request with HTTP connection error")
    void makeGETRequest2() {
        assertThrows(RuntimeException.class, () -> {
            URI uri = new URI("https://www.xdfgdgdfgdf.com/");
            ServiceUtils.makeGETRequest(uri);
        });
    }

}
