package com.cm.ml.infrastructure.utils;

import com.cm.ml.dto.BlacklistDTO;
import com.cm.ml.infrastructure.exceptions.InvalidRequestBodyException;
import io.vertx.rxjava3.ext.web.RoutingContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class APIUtilsTest extends APIUtils {

    @Test
    @DisplayName("Get valid query data for pagination")
    void readPagination1() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getParam(QueryParams.PAGE)).thenReturn("1");
        when(ctx.request().getParam(QueryParams.PAGE_SIZE)).thenReturn("3");

        QueryData queryData = APIUtils.readPagination(ctx);
        assertEquals(queryData.getPage(), 1);
        assertEquals(queryData.getPageSize(), 3);
    }

    @Test
    @DisplayName("Get query data for pagination with invalid parameters")
    void readPagination2() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getParam(QueryParams.PAGE)).thenReturn("sdfsdf");
        when(ctx.request().getParam(QueryParams.PAGE_SIZE)).thenReturn("sfsdfs");

        QueryData queryData = APIUtils.readPagination(ctx);
        assertEquals(queryData.getPage(), 1);
        assertEquals(queryData.getPageSize(), 10);
    }

    @Test
    @DisplayName("Get query data for pagination with null or empty parameters")
    void readPagination3() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getParam(QueryParams.PAGE)).thenReturn(null);
        when(ctx.request().getParam(QueryParams.PAGE_SIZE)).thenReturn("  ");

        QueryData queryData = APIUtils.readPagination(ctx);
        assertEquals(queryData.getPage(), 1);
        assertEquals(queryData.getPageSize(), 10);
    }

    @Test
    @DisplayName("Get string parameter from path")
    void readStringParameter1() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getParam("test")).thenReturn("127.0.0.1");

        String readParameter = APIUtils.extractStringParam(ctx, "test");
        assertEquals(readParameter, "127.0.0.1");
    }

    @Test
    @DisplayName("Get null string parameter from path")
    void readStringParameter2() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getParam("test")).thenReturn(null);

        String readParameter = APIUtils.extractStringParam(ctx, "test");
        assertNull(readParameter);
    }

    @Test
    @DisplayName("Get empty string parameter from path")
    void readStringParameter3() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getParam("test")).thenReturn("   ");

        String readParameter = APIUtils.extractStringParam(ctx, "test");
        assertNull(readParameter);
    }

    @Test
    @DisplayName("Get current timestamp")
    void getCurrentTimestamp1() {
        Timestamp currentTimestamp = APIUtils.getCurrentTimestamp();
        assertTrue(currentTimestamp.getTime() <= new Date().getTime());
    }

    @Test
    @DisplayName("Utilitary method to close connection with null parameters")
    void closeConnection1() {
        assertDoesNotThrow(() -> APIUtils.closeConnection(null, null, new PreparedStatement[]{null}));
    }

    @Test
    @DisplayName("Utilitary method to close connection with null parameters")
    void closeConnection2() throws SQLException {
        Connection c = Mockito.mock(Connection.class, RETURNS_DEEP_STUBS);
        APIUtils.closeConnection(c, null);

        verify(c, times(1)).close();
    }

    @Test
    @DisplayName("Utilitary method to close connection with autocommit false")
    void closeConnection3() throws SQLException {
        Connection c = Mockito.mock(Connection.class, RETURNS_DEEP_STUBS);
        when(c.getAutoCommit()).thenReturn(false);

        APIUtils.closeConnection(c, null);

        verify(c, times(1)).close();
        verify(c, times(1)).rollback();
    }

    @Test
    @DisplayName("Utilitary method to close connection with autocommit true")
    void closeConnection4() throws SQLException {
        Connection c = Mockito.mock(Connection.class, RETURNS_DEEP_STUBS);
        when(c.getAutoCommit()).thenReturn(true);

        APIUtils.closeConnection(c, null);

        verify(c, times(1)).close();
    }

    @Test
    @DisplayName("Utilitary method to close resultset")
    void closeConnection5() throws SQLException {
        ResultSet rs = Mockito.mock(ResultSet.class, RETURNS_DEEP_STUBS);

        APIUtils.closeConnection(null, rs);

        verify(rs, times(1)).close();
    }

    @Test
    @DisplayName("Utilitary method to close prepared statement")
    void closeConnection6() throws SQLException {
        PreparedStatement ps = Mockito.mock(PreparedStatement.class, RETURNS_DEEP_STUBS);

        APIUtils.closeConnection(null, null, ps);

        verify(ps, times(1)).close();
    }

    @Test
    @DisplayName("Utilitary method to close connection generating exception")
    void closeConnection7() {
        assertDoesNotThrow(() -> {
            Connection c = Mockito.mock(Connection.class, RETURNS_DEEP_STUBS);
            doThrow(new SQLException("TEST")).when(c).close();

            APIUtils.closeConnection(c, null);
        });
    }

    @Test
    @DisplayName("Utilitary method to decode JSON with valid body")
    void json1() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.getBodyAsString()).thenReturn("{\"ip\":\"127.0.0.1\"}");

        BlacklistDTO o = APIUtils.json(ctx, BlacklistDTO.class);
        assertEquals(o.ip(), "127.0.0.1");
    }

    @Test
    @DisplayName("Utilitary method to decode JSON with null body")
    void json2() {
        assertThrows(InvalidRequestBodyException.class, () -> {
            RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
            when(ctx.getBodyAsString()).thenReturn(null);

            APIUtils.json(ctx, BlacklistDTO.class);
        });
    }

    @Test
    @DisplayName("Utilitary method to decode JSON with invalid body")
    void json3() {
        assertThrows(InvalidRequestBodyException.class, () -> {
            RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
            when(ctx.getBodyAsString()).thenReturn("{\"ip\":\"127.0.0.\"}");

            APIUtils.json(ctx, BlacklistDTO.class);
        });
    }

}
