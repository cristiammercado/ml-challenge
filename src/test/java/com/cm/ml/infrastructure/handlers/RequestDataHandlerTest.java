package com.cm.ml.infrastructure.handlers;

import io.vertx.rxjava3.ext.web.RoutingContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RequestDataHandlerTest {

    @Test
    @DisplayName("Request data handler for a HTTPS page")
    void handle1() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getHeader("x-forwarded-proto")).thenReturn("https://");

        RequestDataHandler requestDataHandler = new RequestDataHandler();
        requestDataHandler.handle(ctx);

        verify(ctx, times(1)).next();
    }

    @Test
    @DisplayName("Request data handler for a HTTP page")
    void handle2() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getHeader("x-forwarded-proto")).thenReturn("http://");

        RequestDataHandler requestDataHandler = new RequestDataHandler();
        requestDataHandler.handle(ctx);

        verify(ctx, times(1)).next();
    }

    @Test
    @DisplayName("Request data handler for multiple proxy IPs")
    void handle3() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.request().getHeader("x-forwarded-for")).thenReturn("127.0.0.1,.0.0.0.0");

        RequestDataHandler requestDataHandler = new RequestDataHandler();
        requestDataHandler.handle(ctx);

        verify(ctx, times(1)).next();
    }
}
