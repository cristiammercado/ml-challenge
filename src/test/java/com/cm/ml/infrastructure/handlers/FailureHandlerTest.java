package com.cm.ml.infrastructure.handlers;

import com.cm.ml.infrastructure.exceptions.BusinessException;
import io.vertx.rxjava3.ext.web.RoutingContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class FailureHandlerTest {

    @Test
    @DisplayName("Handler for general business exception")
    void handle1() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.failure()).thenReturn(new BusinessException());

        FailureHandler failureHandler = new FailureHandler();
        failureHandler.handle(ctx);

        verify(ctx, times(1)).response();
    }

    @Test
    @DisplayName("Handler for general Throwable")
    void handle2() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.failure()).thenReturn(new Throwable("Test error"));

        FailureHandler failureHandler = new FailureHandler();
        failureHandler.handle(ctx);

        verify(ctx, times(4)).response();
    }

    @Test
    @DisplayName("Handler for general nullish exception")
    void handle3() {
        RoutingContext ctx = Mockito.mock(RoutingContext.class, RETURNS_DEEP_STUBS);
        when(ctx.failure()).thenReturn(null);

        FailureHandler failureHandler = new FailureHandler();
        failureHandler.handle(ctx);

        verify(ctx, times(4)).response();
    }

}
