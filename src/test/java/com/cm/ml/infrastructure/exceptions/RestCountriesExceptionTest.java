package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class RestCountriesExceptionTest {

    @Test
    void createInstance1() {
        RestCountriesException exception = new RestCountriesException();

        assertNotNull(exception);
        assertEquals(exception.getErrorCode(), ErrorCode.EXTERNAL_API_ERROR);
        assertEquals(exception.getStatusCode(), Status.SERVICE_UNAVAILABLE);
    }

    @Test
    void createInstance2() {
        RestCountriesException exception = new RestCountriesException("test message");

        assertNotNull(exception);
        assertEquals(exception.getErrorCode(), ErrorCode.MISSED_INFORMATION);
        assertEquals(exception.getStatusCode(), Status.INTERNAL_SERVER_ERROR);
    }

}
