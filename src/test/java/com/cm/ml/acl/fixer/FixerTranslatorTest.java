package com.cm.ml.acl.fixer;

import com.cm.ml.acl.fixer.domain.ExchangeRates;
import com.cm.ml.infrastructure.exceptions.FixerException;
import com.cm.ml.infrastructure.utils.JsonUtils;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

class FixerTranslatorTest {

    @Test
    @DisplayName("Exception when rates field is null")
    void translate1() {
        assertThrows(FixerException.class, () -> {
            JsonNode jsonNode = Mockito.mock(JsonNode.class, RETURNS_DEEP_STUBS);
            when(jsonNode.hasNonNull("rates")).thenReturn(false);

            FixerTranslator translator = new FixerTranslator();
            translator.translate(anyString(), jsonNode);
        });
    }

    @Test
    @DisplayName("Country currency is USD")
    void translate2() {
        assertDoesNotThrow(() -> {
            JsonNode jsonNode = JsonUtils.decode("{\"success\": true,\"timestamp\": 1624168743,\"base\": \"EUR\",\"date\": \"2021-06-20\",\"rates\": {\"USD\": 1.186435,\"COP\": 4471.673511}}");

            FixerTranslator translator = new FixerTranslator();
            ExchangeRates exchangeRates = translator.translate("USD", jsonNode);

            assertNotNull(exchangeRates);
        });
    }

    @Test
    @DisplayName("Country currency is EUR")
    void translate3() {
        assertDoesNotThrow(() -> {
            JsonNode jsonNode = JsonUtils.decode("{\"success\": true,\"timestamp\": 1624168743,\"base\": \"EUR\",\"date\": \"2021-06-20\",\"rates\": {\"USD\": 1.186435,\"COP\": 4471.673511}}");

            FixerTranslator translator = new FixerTranslator();
            ExchangeRates exchangeRates = translator.translate("EUR", jsonNode);

            assertNotNull(exchangeRates);
        });
    }

    @Test
    @DisplayName("Country currency is USD but USD field is null")
    void translate4() {
        assertThrows(FixerException.class, () -> {
            JsonNode jsonNode = JsonUtils.decode("{\"success\": true,\"timestamp\": 1624168743,\"base\": \"EUR\",\"date\": \"2021-06-20\",\"rates\": {\"USD\": null,\"COP\": 4471.673511}}");

            FixerTranslator translator = new FixerTranslator();
            translator.translate("USD", jsonNode);
        });
    }

    @Test
    @DisplayName("Country currency is EUR but USD field is null")
    void translate5() {
        assertThrows(FixerException.class, () -> {
            JsonNode jsonNode = JsonUtils.decode("{\"success\": true,\"timestamp\": 1624168743,\"base\": \"EUR\",\"date\": \"2021-06-20\",\"rates\": {\"USD\": null,\"COP\": 4471.673511}}");

            FixerTranslator translator = new FixerTranslator();
            translator.translate("EUR", jsonNode);
        });
    }

    @Test
    @DisplayName("USD field is null")
    void translate6() {
        assertThrows(FixerException.class, () -> {
            JsonNode jsonNode = JsonUtils.decode("{\"success\": true,\"timestamp\": 1624168743,\"base\": \"EUR\",\"date\": \"2021-06-20\",\"rates\": {\"USD\": null,\"COP\": 4471.673511}}");

            FixerTranslator translator = new FixerTranslator();
            translator.translate("COP", jsonNode);
        });
    }

    @Test
    @DisplayName("Country currency field is null")
    void translate7() {
        assertThrows(FixerException.class, () -> {
            JsonNode jsonNode = JsonUtils.decode("{\"success\": true,\"timestamp\": 1624168743,\"base\": \"EUR\",\"date\": \"2021-06-20\",\"rates\": {\"USD\": 1.16466,\"COP\": null}}");

            FixerTranslator translator = new FixerTranslator();
            translator.translate("COP", jsonNode);
        });
    }

}
