package com.cm.ml.acl.restcountries;

import com.cm.ml.acl.restcountries.domain.CurrencyInfo;
import com.cm.ml.infrastructure.exceptions.RestCountriesException;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Traductor del dominio del servicio de RestCountries al dominio de la aplicación.
 */
public class RestCountriesTranslator {

    /**
     * Campo de la lista de monedas.
     */
    private static final String CURRENCIES_FIELD = "currencies";

    /**
     * Campo con el código de la moneda.
     */
    private static final String CODE_FIELD = "code";

    /**
     * Convierte del dominio del servicio al dominio de la aplicación.
     *
     * @param json Respuesta de servicio en formato JSON.
     * @return Información de la moneda del país.
     */
    public CurrencyInfo translate(JsonNode json) {

        if (!json.hasNonNull(CURRENCIES_FIELD) || json.get(CURRENCIES_FIELD).get(0) == null) {
            throw new RestCountriesException(CURRENCIES_FIELD);
        }

        JsonNode currenciesNode = json.get(CURRENCIES_FIELD).get(0);

        if (!currenciesNode.hasNonNull(CODE_FIELD)) {
            throw new RestCountriesException(CODE_FIELD);
        }

        JsonNode codeNode = currenciesNode.get(CODE_FIELD);

        return CurrencyInfo.builder()
            .currency(codeNode.asText().toUpperCase())
            .build();
    }


}
