package com.cm.ml.acl.restcountries;

import com.cm.ml.acl.base.ServiceBase;
import com.cm.ml.acl.base.ServiceRegistry;
import com.cm.ml.acl.restcountries.domain.CurrencyInfo;
import com.cm.ml.infrastructure.config.RestCountriesConfig;
import com.cm.ml.infrastructure.exceptions.RestCountriesException;
import com.cm.ml.infrastructure.utils.ServiceUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Servicio para las consultas externas a la API de RestCountries.
 */
@Singleton
public class RestCountriesService extends ServiceBase<JsonNode> {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RestCountriesService.class);

    /**
     * Configuración para este servicio.
     */
    private final RestCountriesConfig config;

    /**
     * Translator para la respuesta del servicio.
     */
    private final RestCountriesTranslator translator;

    @Inject
    public RestCountriesService(ServiceRegistry r, RestCountriesConfig config, RestCountriesTranslator translator) {
        super(r, "rescountries");
        this.config = config;
        this.translator = translator;
    }

    /**
     * Permite consultar la moneda oficial de un país.
     *
     * @param isoCode Código ISO de dos letras del país.
     * @return Información de la moneda del país.
     */
    public CurrencyInfo getCountryCurrency(final String isoCode) {
        try {
            URI uri = this.buildURLQuery(isoCode);
            JsonNode jsonNode = super.decorate("restcountries", isoCode, () -> ServiceUtils.makeGETRequest(uri));

            return this.translator.translate(jsonNode);
        } catch (Exception e) {
            LOGGER.error("Error getting IP country currency", e);
            throw new RestCountriesException();
        }
    }

    /**
     * Permite construir la URL de consulta.
     *
     * @param countryCode Código ISO del país.
     * @return URI para el llamado a la API externa.
     * @throws URISyntaxException Es lanzada si la URL no puede ser parseada como una URI válida.
     */
    private URI buildURLQuery(String countryCode) throws URISyntaxException {

        String url = String.format("%s%s?fields=currencies",
            this.config.getEndpoint(),
            countryCode);

        return new URI(url);
    }

}
