package com.cm.ml.acl.restcountries.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonSerialize(as = CurrencyInfo.class)
@JsonDeserialize(as = CurrencyInfo.class)
public interface AbstractCurrencyInfo {

    String currency();

}
