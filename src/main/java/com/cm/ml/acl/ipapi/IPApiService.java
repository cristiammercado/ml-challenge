package com.cm.ml.acl.ipapi;

import com.cm.ml.acl.base.ServiceBase;
import com.cm.ml.acl.base.ServiceRegistry;
import com.cm.ml.acl.ipapi.domain.CountryInfo;
import com.cm.ml.infrastructure.config.IPApiConfig;
import com.cm.ml.infrastructure.exceptions.IPApiException;
import com.cm.ml.infrastructure.utils.ServiceUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Servicio para las consultas externas a la API de IPApi.
 */
@Singleton
public class IPApiService extends ServiceBase<JsonNode> {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(IPApiService.class);

    /**
     * Configuración para este servicio.
     */
    private final IPApiConfig config;

    /**
     * Translator para la respuesta del servicio.
     */
    private final IPApiTranslator translator;

    @Inject
    public IPApiService(ServiceRegistry r, IPApiConfig config, IPApiTranslator translator) {
        super(r, "ipapi");
        this.config = config;
        this.translator = translator;
    }

    /**
     * Permite consultar la información de país por IP.
     *
     * @param ip IP a consultar.
     * @return Información del país.
     */
    public CountryInfo getIPCountry(final String ip) {
        try {
            URI uri = this.buildURLQuery(ip);
            JsonNode jsonNode = super.decorate("ipapi", ip, () -> ServiceUtils.makeGETRequest(uri));

            return this.translator.translate(jsonNode);
        } catch (Exception e) {
            LOGGER.error("Error getting IP country", e);
            throw new IPApiException();
        }
    }

    /**
     * Permite construir la URL de consulta.
     *
     * @param ipLookup IP a buscar info.
     * @return URI para el llamado a la API externa.
     * @throws URISyntaxException Es lanzada si la URL no puede ser parseada como una URI válida.
     */
    private URI buildURLQuery(String ipLookup) throws URISyntaxException {

        String url = String.format("%s%s?access_key=%s&output=json&fields=ip,country_code,country_name",
            this.config.getEndpoint(),
            ipLookup,
            this.config.getToken());

        return new URI(url);
    }

}
