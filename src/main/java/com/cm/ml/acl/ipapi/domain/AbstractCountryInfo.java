package com.cm.ml.acl.ipapi.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonSerialize(as = CountryInfo.class)
@JsonDeserialize(as = CountryInfo.class)
public interface AbstractCountryInfo {

    String ip();

    String countryCode();

    String countryName();

}
