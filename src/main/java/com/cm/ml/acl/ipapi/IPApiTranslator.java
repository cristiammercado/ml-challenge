package com.cm.ml.acl.ipapi;

import com.cm.ml.acl.ipapi.domain.CountryInfo;
import com.cm.ml.infrastructure.exceptions.IPApiException;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Traductor del dominio del servicio de IPApi al dominio de la aplicación.
 */
public class IPApiTranslator {

    /**
     * Campo de IP.
     */
    private static final String IP_FIELD = "ip";

    /**
     * Campo del código de país.
     */
    private static final String COUNTRY_CODE_FIELD = "country_code";

    /**
     * Campo del nombre de país.
     */
    private static final String COUNTRY_NAME_FIELD = "country_name";

    /**
     * Convierte del dominio del servicio al dominio de la aplicación.
     *
     * @param json Respuesta de servicio en formato JSON.
     * @return Información del país de la IP.
     */
    public CountryInfo translate(JsonNode json) {

        if (!json.hasNonNull(IP_FIELD)) {
            throw new IPApiException(IP_FIELD);
        }

        if (!json.hasNonNull(COUNTRY_CODE_FIELD)) {
            throw new IPApiException(COUNTRY_CODE_FIELD);
        }

        if (!json.hasNonNull(COUNTRY_NAME_FIELD)) {
            throw new IPApiException(COUNTRY_NAME_FIELD);
        }

        JsonNode ipNode = json.get(IP_FIELD);
        JsonNode countryCodeNode = json.get(COUNTRY_CODE_FIELD);
        JsonNode countryNameNode = json.get(COUNTRY_NAME_FIELD);

        return CountryInfo.builder()
            .ip(ipNode.asText())
            .countryCode(countryCodeNode.asText())
            .countryName(countryNameNode.asText())
            .build();
    }


}
