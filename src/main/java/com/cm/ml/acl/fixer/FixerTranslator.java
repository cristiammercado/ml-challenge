package com.cm.ml.acl.fixer;

import com.cm.ml.acl.fixer.domain.ExchangeRates;
import com.cm.ml.infrastructure.exceptions.FixerException;
import com.fasterxml.jackson.databind.JsonNode;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Traductor del dominio del servicio de IPApi al dominio de la aplicación.
 */
public class FixerTranslator {

    /**
     * Campo de la lista de tasas de cambio.
     */
    private static final String RATES_FIELD = "rates";

    /**
     * Campo del código de país.
     */
    private static final String USD_FIELD = "USD";

    /**
     * Campo del código de país.
     */
    private static final String EUR_FIELD = "EUR";

    /**
     * Convierte del dominio del servicio al dominio de la aplicación.
     *
     * @param countryCurrency Moneda base del país.
     * @param json            Respuesta de servicio en formato JSON.
     * @return Información de las tasas de cambio.
     */
    public ExchangeRates translate(String countryCurrency, JsonNode json) {

        if (!json.hasNonNull(RATES_FIELD)) {
            throw new FixerException(RATES_FIELD);
        }

        JsonNode ratesNode = json.get(RATES_FIELD);

        BigDecimal eurRate;
        BigDecimal usdRate;

        if (USD_FIELD.equals(countryCurrency)) {
            usdRate = BigDecimal.ONE;

            if (!ratesNode.hasNonNull(USD_FIELD)) {
                throw new FixerException(USD_FIELD);
            }

            JsonNode usdNode = ratesNode.get(USD_FIELD);
            BigDecimal eur = BigDecimal.valueOf(usdNode.asDouble());

            eurRate = usdRate.divide(eur, 5, RoundingMode.HALF_EVEN);
        } else if (EUR_FIELD.equals(countryCurrency)) {
            eurRate = BigDecimal.ONE;

            if (!ratesNode.hasNonNull(USD_FIELD)) {
                throw new FixerException(USD_FIELD);
            }

            JsonNode usdNode = ratesNode.get(USD_FIELD);
            usdRate = BigDecimal.valueOf(usdNode.asDouble());
        } else {

            if (!ratesNode.hasNonNull(USD_FIELD)) {
                throw new FixerException(USD_FIELD);
            }

            if (!ratesNode.hasNonNull(countryCurrency)) {
                throw new FixerException(countryCurrency);
            }

            JsonNode usdNode = ratesNode.get(USD_FIELD);
            BigDecimal usd = BigDecimal.valueOf(usdNode.asDouble());

            JsonNode countryNode = ratesNode.get(countryCurrency);
            BigDecimal country = BigDecimal.valueOf(countryNode.asDouble());

            eurRate = BigDecimal.ONE.divide(country, 5, RoundingMode.HALF_EVEN);
            usdRate = BigDecimal.ONE.divide(country, 5, RoundingMode.HALF_EVEN).multiply(usd);
        }

        return ExchangeRates.builder()
            .eur(eurRate.setScale(5, RoundingMode.HALF_EVEN))
            .usd(usdRate.setScale(5, RoundingMode.HALF_EVEN))
            .build();
    }


}
