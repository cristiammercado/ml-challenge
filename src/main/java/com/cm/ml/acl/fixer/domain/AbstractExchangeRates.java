package com.cm.ml.acl.fixer.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.math.BigDecimal;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonSerialize(as = ExchangeRates.class)
@JsonDeserialize(as = ExchangeRates.class)
public interface AbstractExchangeRates {

    BigDecimal usd();

    BigDecimal eur();

}
