package com.cm.ml.acl.fixer;

import com.cm.ml.acl.base.ServiceBase;
import com.cm.ml.acl.base.ServiceRegistry;
import com.cm.ml.acl.fixer.domain.ExchangeRates;
import com.cm.ml.infrastructure.config.FixerConfig;
import com.cm.ml.infrastructure.exceptions.FixerException;
import com.cm.ml.infrastructure.utils.ServiceUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Servicio para las consultas externas a la API de Fixer.
 */
@Singleton
public class FixerService extends ServiceBase<JsonNode> {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FixerService.class);

    /**
     * Configuración para este servicio.
     */
    private final FixerConfig config;

    /**
     * Translator para la respuesta del servicio.
     */
    private final FixerTranslator translator;

    @Inject
    public FixerService(ServiceRegistry r, FixerConfig config, FixerTranslator translator) {
        super(r, "fixer");
        this.config = config;
        this.translator = translator;
    }

    /**
     * Permite consultar la tasa de cambio de la moneda base hacia EUR y USD.
     *
     * @param countryCurrency Moneda base del país.
     * @return Información de las tasas de cambios hacia EUR y USD.
     */
    public ExchangeRates getExchangeData(final String countryCurrency) {
        try {
            URI uri = this.buildURLQuery(countryCurrency);
            JsonNode jsonNode = super.decorate("fixer", countryCurrency, () -> ServiceUtils.makeGETRequest(uri));

            return this.translator.translate(countryCurrency, jsonNode);
        } catch (Exception e) {
            LOGGER.error("Error getting IP country exchange rates", e);
            throw new FixerException();
        }
    }

    /**
     * Permite construir la URL de consulta.
     *
     * @param currencyCode Moneda base del país.
     * @return URI para el llamado a la API externa.
     * @throws URISyntaxException Es lanzada si la URL no puede ser parseada como una URI válida.
     */
    private URI buildURLQuery(String currencyCode) throws URISyntaxException {

        String url = String.format("%s?access_key=%s&base=EUR&symbols=USD,%s",
            this.config.getEndpoint(),
            this.config.getToken(),
            currencyCode);

        return new URI(url);
    }

}
