package com.cm.ml.acl.base;

import com.google.inject.Inject;
import io.github.resilience4j.cache.Cache;
import io.github.resilience4j.decorators.Decorators;

import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;
import java.util.function.Supplier;

/**
 * Clase base para todos los servicios a APIs externas usadas en la aplicación.
 */
public class ServiceBase<T> {

    /**
     * Instancia con los registros de los decoradores usados.
     */
    private final ServiceRegistry registry;

    /**
     * Caché manager.
     */
    private final Cache<String, T> cacheContext;

    /**
     * Constructor.
     *
     * @param registry Nombre del registro del servicio.
     * @param cacheKey Key para el administrador de caché.
     */
    @Inject
    public ServiceBase(ServiceRegistry registry, String cacheKey) {
        this.registry = registry;

        CacheManager cacheManager = Caching.getCachingProvider().getCacheManager();
        MutableConfiguration<String, T> config = new MutableConfiguration<>();
        config.setExpiryPolicyFactory(() -> new CreatedExpiryPolicy(Duration.ONE_DAY));

        if (cacheManager.getCache(cacheKey) != null) {
            this.cacheContext = Cache.of(cacheManager.getCache(cacheKey));
        } else {
            this.cacheContext = Cache.of(cacheManager.createCache(cacheKey, config));
        }
    }

    /**
     * Decora el request externo con un circuit breaker, bulkhead, retry y caché.
     *
     * @param key      Llave que identifica al servicio.
     * @param cacheKey Key específica para la caché.
     * @param supplier Función a ser decorada.
     * @return Resultado de la operación.
     */
    public T decorate(String key, String cacheKey, Supplier<T> supplier) {

        return Decorators.ofSupplier(supplier)
            .withCircuitBreaker(this.registry.getCB(key))
            .withBulkhead(this.registry.getBH(key))
            .withRetry(this.registry.getRT(key))
            .withCache(this.cacheContext)
            .decorate()
            .apply(cacheKey);
    }

}
