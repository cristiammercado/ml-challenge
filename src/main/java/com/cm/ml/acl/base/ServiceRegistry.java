package com.cm.ml.acl.base;

import com.cm.ml.infrastructure.exceptions.BusinessException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.bulkhead.BulkheadConfig;
import io.github.resilience4j.bulkhead.BulkheadRegistry;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;

import java.time.Duration;

/**
 * Clase que lleva el registro de circuit breakers, bulkheads y retries usados en los servicios externos a la API.
 */
@Singleton
public class ServiceRegistry {

    /**
     * Registro de CircuitBreakers usados.
     */
    private final CircuitBreakerRegistry circuitBreakerRegistry;

    /**
     * Registro de Bulkheads usados.
     */
    private final BulkheadRegistry bulkheadRegistry;

    /**
     * Registro de Retries usados.
     */
    private final RetryRegistry retryRegistry;

    @Inject
    public ServiceRegistry() {

        CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
            .failureRateThreshold(95)
            .slowCallRateThreshold(95)
            .slowCallDurationThreshold(Duration.ofMillis(20000))
            .permittedNumberOfCallsInHalfOpenState(10)
            .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
            .slidingWindowSize(10)
            .minimumNumberOfCalls(10)
            .waitDurationInOpenState(Duration.ofMillis(5000))
            .build();

        this.circuitBreakerRegistry = CircuitBreakerRegistry.of(circuitBreakerConfig);

        BulkheadConfig bulkheadConfig = BulkheadConfig.custom()
            .maxConcurrentCalls(150)
            .maxWaitDuration(Duration.ofMillis(1000))
            .fairCallHandlingStrategyEnabled(true)
            .build();

        this.bulkheadRegistry = BulkheadRegistry.of(bulkheadConfig);

        RetryConfig retryConfig = RetryConfig.custom()
            .maxAttempts(2)
            .waitDuration(Duration.ofMillis(1000))
            .retryExceptions(RuntimeException.class)
            .ignoreExceptions(BusinessException.class)
            .build();

        this.retryRegistry = RetryRegistry.of(retryConfig);
    }

    /**
     * Obtiene el CircuitBreaker asociado al nombre recibido por parámetro.
     *
     * @param name Nombre del CircuitBreaker.
     * @return Instancia del CircuitBreaker asociado.
     */
    public CircuitBreaker getCB(String name) {
        return this.circuitBreakerRegistry.circuitBreaker(name);
    }

    /**
     * Obtiene el Bulkhead asociado al nombre recibido por parámetro.
     *
     * @param name Nombre del Bulkhead.
     * @return Instancia del Bulkhead asociado.
     */
    public Bulkhead getBH(String name) {
        return this.bulkheadRegistry.bulkhead(name);
    }

    /**
     * Obtiene el Retry asociado al nombre recibido por parámetro.
     *
     * @param name Nombre del Retry.
     * @return Instancia del Retry asociado.
     */
    public Retry getRT(String name) {
        return this.retryRegistry.retry(name);
    }

}
