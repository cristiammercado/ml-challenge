package com.cm.ml.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = ListDTO.class)
@JsonDeserialize(as = ListDTO.class)
public interface List {

    Integer records();

    java.util.List<Object> list();

}
