package com.cm.ml.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.Date;

@Value.Immutable
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = APIStatusDTO.class)
@JsonDeserialize(as = APIStatusDTO.class)
public interface APIStatus {

    @Value.Default
    default String text() {
        return "API is working correctly";
    }

    @Value.Default
    default Date date() {
        return new Date();
    }

}
