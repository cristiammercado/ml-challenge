package com.cm.ml.dto;

import com.cm.ml.infrastructure.exceptions.ErrorResponse;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = APIResponseDTO.class)
@JsonDeserialize(as = APIResponseDTO.class)
public interface APIResponse {

    Optional<ErrorResponse> error();

    Optional<Object> data();

    Boolean success();

}
