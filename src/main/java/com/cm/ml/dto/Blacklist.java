package com.cm.ml.dto;

import com.cm.ml.infrastructure.utils.Validators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = BlacklistDTO.class)
@JsonDeserialize(as = BlacklistDTO.class)
public interface Blacklist {

    String ip();

    @Value.Check
    default void check() {
        Preconditions.checkState(Validators.isIPValid(ip()), "'ip' is invalid, please check format and try again");
    }

}
