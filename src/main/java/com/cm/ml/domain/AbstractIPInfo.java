package com.cm.ml.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.math.BigDecimal;
import java.util.Optional;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonSerialize(as = IPInfo.class)
@JsonDeserialize(as = IPInfo.class)
public interface AbstractIPInfo {

    String ip();

    String country();

    String isoCode();

    String currency();

    BigDecimal usdExchangeRate();

    BigDecimal eurExchangeRate();

}
