package com.cm.ml.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.sql.Timestamp;

@Value.Immutable
@Value.Style(typeImmutable = "*")
@JsonSerialize(as = Blacklist.class)
@JsonDeserialize(as = Blacklist.class)
public interface AbstractBlacklist {

    String ip();

    Timestamp createdAt();

}
