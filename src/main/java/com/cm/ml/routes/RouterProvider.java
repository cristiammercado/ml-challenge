package com.cm.ml.routes;

import com.cm.ml.controllers.BlacklistController;
import com.cm.ml.controllers.IPController;
import com.cm.ml.controllers.UpController;
import com.cm.ml.infrastructure.config.CORSConfig;
import com.cm.ml.infrastructure.handlers.FailureHandler;
import com.cm.ml.infrastructure.handlers.HeaderHandler;
import com.cm.ml.infrastructure.handlers.NotFoundHandler;
import com.cm.ml.infrastructure.handlers.RequestDataHandler;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import io.vertx.core.http.HttpMethod;
import io.vertx.rxjava3.core.Vertx;
import io.vertx.rxjava3.ext.web.Router;
import io.vertx.rxjava3.ext.web.handler.BodyHandler;
import io.vertx.rxjava3.ext.web.handler.CorsHandler;

/**
 * Clase que provee la configuración de las rutas de la API.
 */
@Singleton
public class RouterProvider implements Provider<Router> {

    /**
     * Instancia de Vertx.
     */
    private final Vertx vertx;

    /**
     * Configuración de CORS.
     */
    private final CORSConfig CORSConfig;

    /**
     * Handler para excepciones generales.
     */
    private final FailureHandler failureHandler;

    /**
     * Handler para encabezados HTTP.
     */
    private final HeaderHandler headerHandler;

    /**
     * Handler para rutas no encontradas.
     */
    private final NotFoundHandler notFoundHandler;

    /**
     * Handler para logging de requests.
     */
    private final RequestDataHandler requestDataHandler;

    /**
     * Controlador: status.
     */
    private final UpController upCtrl;

    /**
     * Controlador: IP.
     */
    private final IPController ipCtrl;

    /**
     * Controlador: Blacklist.
     */
    private final BlacklistController blacklistCtrl;

    /**
     * Constructor para esta clase.
     */
    @Inject
    public RouterProvider(Vertx vertx,
                          com.cm.ml.infrastructure.config.CORSConfig CORSConfig,
                          FailureHandler failureHandler,
                          HeaderHandler headerHandler,
                          NotFoundHandler notFoundHandler,
                          RequestDataHandler requestDataHandler,
                          UpController upCtrl,
                          IPController ipCtrl,
                          BlacklistController blacklistCtrl) {

        this.vertx = vertx;
        this.CORSConfig = CORSConfig;
        this.failureHandler = failureHandler;
        this.headerHandler = headerHandler;
        this.notFoundHandler = notFoundHandler;
        this.requestDataHandler = requestDataHandler;
        this.upCtrl = upCtrl;
        this.ipCtrl = ipCtrl;
        this.blacklistCtrl = blacklistCtrl;
    }

    @Override
    public Router get() {

        // configura ruteador principal
        Router router = Router.router(vertx);

        // configure manejadores
        setHandlers(router);

        //configura las rutas de la API
        setRoutes(router);

        // handler para excepciones en todas las rutas
        router.route().failureHandler(failureHandler);

        // handler para errores de tipo NOT FOUND
        router.route().last().handler(notFoundHandler);

        // retorna ruteador principal
        return router;
    }

    /**
     * Setea los diferentes handlers de la API.
     *
     * @param router Instancia del ruteador de la API (Router).
     */
    private void setHandlers(Router router) {

        // handler para configurar encabezados HTTP en todas las respuestas
        router.route().handler(headerHandler);

        // handler para imprimir request en el log
        router.route().handler(requestDataHandler);

        // handler para permitir cross-origin (AJAX)
        CorsHandler corsHandler = CorsHandler.create(CORSConfig.getAllowedOrigins());
        corsHandler.allowCredentials(CORSConfig.isAllowCredentials());
        CORSConfig.getAllowedHttpMethods().forEach(corsHandler::allowedMethod);
        corsHandler.allowedHeaders(CORSConfig.getAllowedHttpHeaders());
        corsHandler.exposedHeaders(CORSConfig.getExposedHeaders());
        corsHandler.maxAgeSeconds(CORSConfig.getMaxAge());
        router.route().handler(corsHandler);

        // handler para capturar el contenido
        router.route().handler(BodyHandler
            .create()
            .setBodyLimit(-1)
            .setDeleteUploadedFilesOnEnd(false));
    }

    /**
     * Setea en el enrutador todas las rutas de la API.
     *
     * @param router Instancia del router.
     */
    private void setRoutes(Router router) {

        // MÓDULO STATUS
        router.route(HttpMethod.GET, "/status").handler(upCtrl::status);
        router.route(HttpMethod.HEAD, "/status").handler(upCtrl::status);

        // MÓDULO IP
        router.route(HttpMethod.GET, "/ip").handler(ipCtrl::getInfoFromRequest);
        router.route(HttpMethod.GET, "/ip/:ip").handler(ipCtrl::getInfoFromURL);

        // MÓDULO BLACKLIST
        router.route(HttpMethod.GET, "/blacklist").handler(blacklistCtrl::list);
        router.route(HttpMethod.POST, "/blacklist").handler(blacklistCtrl::post);
        router.route(HttpMethod.DELETE, "/blacklist/:ip").handler(blacklistCtrl::delete);
    }

}
