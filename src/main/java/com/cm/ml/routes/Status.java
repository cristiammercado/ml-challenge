package com.cm.ml.routes;

/**
 * Lista de códigos de estado HTTP.
 */
public class Status {

    /**
     * OK.
     */
    public static final int OK = 200;

    /**
     * CREATED.
     */
    public static final int CREATED = 201;

    /**
     * BAD REQUEST.
     */
    public static final int BAD_REQUEST = 400;

    /**
     * FORBIDDEN.
     */
    public static final int FORBIDDEN = 403;

    /**
     * NOT FOUND.
     */
    public static final int NOT_FOUND = 404;

    /**
     * CONFLICT.
     */
    public static final int CONFLICT = 409;

    /**
     * UNPROCCESSABLE ENTITY.
     */
    public static final int UNPROCCESSABLE_ENTITY = 422;

    /**
     * INTERNAL SERVER ERROR.
     */
    public static final int INTERNAL_SERVER_ERROR = 500;

    /**
     * SERVICE UNAVAILABLE.
     */
    public static final int SERVICE_UNAVAILABLE = 503;

}
