package com.cm.ml.dao;

import com.cm.ml.domain.Blacklist;
import com.cm.ml.infrastructure.database.DatabaseConnection;
import com.cm.ml.infrastructure.exceptions.BusinessException;
import com.cm.ml.infrastructure.exceptions.CreateConflictException;
import com.cm.ml.infrastructure.exceptions.UpdateConflictException;
import com.cm.ml.infrastructure.utils.APIUtils;
import com.cm.ml.infrastructure.utils.QueryData;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementación de los métodos del DAO para el módulo de Blacklist.
 */
@Singleton
public class BlacklistDAO {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BlacklistDAO.class);

    /**
     * Instancia de la conexión con la base de datos.
     */
    private final DatabaseConnection connection;

    /**
     * Constructor por parámetros para esta clase.
     *
     * @param connection Instancia de la conexión con la base de datos.
     */
    @Inject
    public BlacklistDAO(DatabaseConnection connection) {
        this.connection = connection;
    }

    /**
     * Permite listar mediante paginación todos los registros en la blacklist.
     *
     * @param qd Instancia con la información proveniente de la query de un endpoint.
     * @return Instancia con los resultados de la operación.
     */
    public List<Blacklist> listByPage(QueryData qd) {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Blacklist> blacklists = new ArrayList<>();

        try {
            c = connection.get();
            ps = c.prepareStatement("SELECT ip, created_at FROM blacklist ORDER BY created_at DESC LIMIT ? OFFSET ?");

            ps.setInt(1, qd.getPageSize());
            ps.setInt(2, (qd.getPage() - 1) * qd.getPageSize());

            rs = ps.executeQuery();

            while (rs.next()) {
                Blacklist blacklist = Blacklist.builder()
                    .ip(rs.getString("ip"))
                    .createdAt(rs.getTimestamp("created_at"))
                    .build();

                blacklists.add(blacklist);
            }

            return blacklists;
        } catch (Exception e) {
            LOGGER.error("Error listing blacklist by page", e);
            throw new BusinessException();
        } finally {
            APIUtils.closeConnection(c, rs, ps);
        }
    }

    /**
     * Inserta una IP en la blacklist.
     *
     * @param ip Instancia con los datos del recurso a crear.
     */
    public void create(String ip) {
        Connection c = null;
        PreparedStatement ps = null;

        try {
            c = connection.get();
            c.setAutoCommit(false);

            ps = c.prepareStatement("INSERT INTO blacklist (ip, created_at) VALUES (?, ?) ON DUPLICATE KEY UPDATE created_at = ?");

            ps.setString(1, ip);
            ps.setTimestamp(2, APIUtils.getCurrentTimestamp());
            ps.setTimestamp(3, APIUtils.getCurrentTimestamp());

            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            throw new CreateConflictException(e.getMessage());
        } finally {
            APIUtils.closeConnection(c, null, ps);
        }
    }

    /**
     * Elimina una IP de la blacklist.
     *
     * @param ip Instancia con los datos del recurso a crear.
     */
    public void delete(String ip) {
        Connection c = null;
        PreparedStatement ps = null;

        try {
            c = connection.get();
            c.setAutoCommit(false);

            ps = c.prepareStatement("DELETE FROM blacklist WHERE ip = ?");

            ps.setString(1, ip);

            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            throw new UpdateConflictException(e.getMessage());
        } finally {
            APIUtils.closeConnection(c, null, ps);
        }
    }

    /**
     * Elimina una IP de la blacklist.
     *
     * @param ip Instancia con los datos del recurso a crear.
     */
    public boolean isIPBlacklisted(String ip) {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs;

        try {
            c = connection.get();

            ps = c.prepareStatement("SELECT ip FROM blacklist WHERE ip = ?");
            ps.setString(1, ip);

            rs = ps.executeQuery();

            return rs.next();
        } catch (Exception e) {
            LOGGER.error("Error getting ip from blacklist", e);
            throw new BusinessException();
        } finally {
            APIUtils.closeConnection(c, null, ps);
        }
    }

}
