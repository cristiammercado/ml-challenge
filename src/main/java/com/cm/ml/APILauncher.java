package com.cm.ml;

import com.cm.ml.infrastructure.config.APIConfig;
import com.cm.ml.infrastructure.config.DeploymentConfig;
import com.cm.ml.infrastructure.config.EnvConfig;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Launcher;
import io.vertx.core.VertxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Clase de arranque del servidor de la API.
 */
public class APILauncher extends Launcher {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(APILauncher.class);

    /**
     * Ruta a nivel de paquete del principal verticle de la API.
     */
    private static final String MAIN_VERTICLE = MainVerticle.class.getName();

    /**
     * Configuración de despliegue.
     */
    @Inject
    private DeploymentConfig deploymentConfig;

    /**
     * Función de arranque de la aplicación.
     *
     * @param args Argumentos recibidos por consola.
     */
    public static void main(String[] args) {
        initConfiguration(args);

        new APILauncher().dispatch(args);
    }

    /**
     * Permite detectar en que ambiente se está corriendo la aplicación.
     *
     * @param args Argumentos recibidos por consola.
     */
    private static void initConfiguration(String[] args) {

        String env = Arrays.stream(args)
            .filter(Objects::nonNull)
            .filter(s -> !s.isEmpty())
            .filter(s -> s.contains("--env="))
            .map(s -> s.replace("--env=", "").toLowerCase())
            .collect(Collectors.joining());

        EnvConfig.get().set(env);
    }

    @Override
    public void beforeStartingVertx(VertxOptions options) {
        LOGGER.info("Configuring Vert.x options...");

        // crea un injector para esta instancia
        LOGGER.info("Starting Google Guice...");
        Injector injector = Guice.createInjector(new APIConfig());
        injector.injectMembers(this);

        // Ajustamos opciones de Vertx
        options = options
            .setEventLoopPoolSize(2)
            .setInternalBlockingPoolSize(2)
            .setBlockedThreadCheckInterval(TimeUnit.SECONDS.toMillis(2))
            .setBlockedThreadCheckIntervalUnit(TimeUnit.MILLISECONDS)
            .setWorkerPoolSize(2)
            .setMaxWorkerExecuteTime(TimeUnit.SECONDS.toMillis(60))
            .setMaxWorkerExecuteTimeUnit(TimeUnit.MILLISECONDS);

        super.beforeStartingVertx(options);
    }

    @Override
    public void beforeDeployingVerticle(DeploymentOptions deploymentOptions) {
        LOGGER.info("Number of started verticles: " + this.deploymentConfig.getVerticleInstances());

        deploymentOptions = deploymentOptions
            .setInstances(this.deploymentConfig.getVerticleInstances())
            .setWorker(false)
            .setWorkerPoolName("api.worker")
            .setWorkerPoolSize(5)
            .setMaxWorkerExecuteTimeUnit(TimeUnit.MILLISECONDS)
            .setMaxWorkerExecuteTime(TimeUnit.SECONDS.toMillis(60));

        super.beforeDeployingVerticle(deploymentOptions);
    }

    @Override
    protected String getMainVerticle() {
        return MAIN_VERTICLE;
    }

}
