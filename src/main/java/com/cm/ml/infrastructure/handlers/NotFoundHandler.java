package com.cm.ml.infrastructure.handlers;

import com.cm.ml.dto.APIResponseDTO;
import com.cm.ml.infrastructure.exceptions.ErrorCode;
import com.cm.ml.infrastructure.exceptions.ErrorResponse;
import com.cm.ml.infrastructure.utils.JsonUtils;
import com.cm.ml.routes.Status;
import io.vertx.core.Handler;
import io.vertx.rxjava3.ext.web.RoutingContext;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * Provee un manejo genérico de excepciones generadas en la API.
 */
public class NotFoundHandler implements Handler<RoutingContext> {

    /**
     * Realiza la lógica de manejo de excepciones
     *
     * @param ctx Contexto de la petición
     */
    @Override
    public void handle(RoutingContext ctx) {
        ErrorResponse errorResponse = new ErrorResponse(ErrorCode.URL_OR_RESOURCE_NOT_FOUND, "URL or resource not found");
        this.writeResponse(ctx, errorResponse);
    }

    /**
     * Escribe la respuesta de error al usuario final de la API.
     *
     * @param ctx           Routing Context.
     * @param errorResponse Respuesta de error amigable para el usuario final.
     */
    private void writeResponse(RoutingContext ctx, ErrorResponse errorResponse) {

        APIResponseDTO response = APIResponseDTO.builder()
            .data(Optional.empty())
            .error(Optional.ofNullable(errorResponse))
            .success(false)
            .build();

        ctx.response()
            .setStatusCode(Status.NOT_FOUND)
            .end(JsonUtils.encode(response), StandardCharsets.UTF_8.name());
    }

}
