package com.cm.ml.infrastructure.handlers;

import com.cm.ml.infrastructure.common.APIConstants;
import com.google.inject.Singleton;
import io.vertx.core.Handler;
import io.vertx.rxjava3.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Imprime en el log los requests que se realizan a la API.
 */
@Singleton
public class RequestDataHandler implements Handler<RoutingContext> {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestDataHandler.class);

    /**
     * Realiza la lógica para guardar el último request realizado.
     */
    @Override
    public void handle(RoutingContext ctx) {
        String protocol = ctx.request().getHeader("x-forwarded-proto");
        String ip = ctx.request().getHeader("x-forwarded-for");

        String method = ctx.request().method().name();
        String host = ctx.request().host();
        String path = ctx.request().path();
        String queryString = ctx.request().query();

        if (protocol == null || protocol.trim().isEmpty()) {
            protocol = "http";
        } else {
            if (protocol.contains("https")) {
                protocol = "https";
            } else {
                protocol = "http";
            }
        }

        if (ip == null || ip.trim().isEmpty()) {
            ip = "127.0.0.1";
        }

        if (ip.split(",").length > 1) {
            ip = String.valueOf(ip.split(",")[0]).trim();
        }

        if (queryString == null || queryString.trim().isEmpty()) {
            queryString = "";
        }

        String url = protocol + "://" + host + path + queryString;

        LOGGER.info("Request => [" + ip + "] " + method + " " + url);

        ctx.put(APIConstants.REQUEST_IP, ip);
        ctx.next();
    }

}
