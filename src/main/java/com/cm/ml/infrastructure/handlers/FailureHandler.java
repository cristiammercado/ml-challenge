package com.cm.ml.infrastructure.handlers;

import com.cm.ml.dto.APIResponseDTO;
import com.cm.ml.infrastructure.exceptions.BusinessException;
import com.cm.ml.infrastructure.exceptions.ErrorCode;
import com.cm.ml.infrastructure.exceptions.ErrorResponse;
import com.cm.ml.infrastructure.utils.JsonUtils;
import io.vertx.core.Handler;
import io.vertx.rxjava3.ext.web.RoutingContext;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * Provee un manejo genérico de excepciones generadas en la API.
 */
public class FailureHandler implements Handler<RoutingContext> {

    /**
     * Realiza la lógica de manejo de excepciones
     *
     * @param ctx Contexto de la petición
     */
    @Override
    public void handle(RoutingContext ctx) {

        if (ctx.failure() instanceof BusinessException) {
            BusinessException ex = (BusinessException) ctx.failure();

            ErrorResponse errorResponse = new ErrorResponse(ex);
            this.writeResponse(ctx, ex.getStatusCode(), errorResponse);
        } else {
            Throwable exception = ctx.failure();
            int statusCode = ctx.response().getStatusCode() == 200 ? 500 : ctx.response().getStatusCode();
            String statusMessage = ctx.response().getStatusMessage();

            if (exception == null) {
                BusinessException apiException = new BusinessException(statusMessage, ErrorCode.GENERIC_ERROR, statusCode);

                ErrorResponse errorResponse = new ErrorResponse(apiException);
                this.writeResponse(ctx, apiException.getStatusCode(), errorResponse);
            } else {

                ErrorResponse errorResponse = new ErrorResponse(ErrorCode.GENERIC_ERROR, exception.getMessage() != null ? exception.getMessage() : exception.toString());
                this.writeResponse(ctx, statusCode, errorResponse);
            }

        }

    }

    /**
     * Escribe la respuesta de error al usuario final de la API.
     *
     * @param ctx           Routing Context.
     * @param statusCode    Código de estado HTTP.
     * @param errorResponse Respuesta de error amigable para el usuario final.
     */
    private void writeResponse(RoutingContext ctx, int statusCode, ErrorResponse errorResponse) {

        APIResponseDTO response = APIResponseDTO.builder()
            .data(Optional.empty())
            .error(Optional.ofNullable(errorResponse))
            .success(statusCode >= 200 && statusCode < 400)
            .build();

        ctx.response()
            .setStatusCode(statusCode)
            .end(JsonUtils.encode(response), StandardCharsets.UTF_8.name());
    }

}
