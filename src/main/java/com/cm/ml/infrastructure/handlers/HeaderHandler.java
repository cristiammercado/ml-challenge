package com.cm.ml.infrastructure.handlers;

import com.cm.ml.infrastructure.config.DeploymentConfig;
import com.google.inject.Inject;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.rxjava3.ext.web.RoutingContext;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Provee un manejo genérico de los encabezados HTTP por defecto generados para las solicitudes a la API.
 */
public class HeaderHandler implements Handler<RoutingContext> {

    /**
     * Configuración de despliegue.
     */
    private final DeploymentConfig config;

    /**
     * Configuración de fecha.
     */
    private final SimpleDateFormat simpleDateFormat;

    /**
     * Constructor para esta clase.
     *
     * @param config Configuración de despliegue.
     */
    @Inject
    public HeaderHandler(DeploymentConfig config) {
        this.config = config;

        this.simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", new Locale("en"));
        this.simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    /**
     * Realiza la lógica para generar los encabezados HTTP de respuesta.
     *
     * @param ctx Routing context.
     */
    @Override
    public void handle(RoutingContext ctx) {

        // seteamos los encabezados HTTP que aplican a todas la rutas de la API
        ctx.response()
            .putHeader(HttpHeaders.DATE, this.simpleDateFormat.format(new Date()))
            .putHeader(HttpHeaders.CONNECTION, "keep-alive")
            .putHeader(HttpHeaders.KEEP_ALIVE, "timeout=" + this.config.getKeepAliveTimeout() + ", max=" + this.config.getKeepAliveMax())
            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");

        //Llamamos al siguiente handler
        ctx.next();
    }

}
