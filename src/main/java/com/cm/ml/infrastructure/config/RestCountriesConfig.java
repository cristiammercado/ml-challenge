package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;

/**
 * Clase que contiene la configuración para consultar el servicio externo: RestCountries.
 */
@Singleton
public class RestCountriesConfig {

    private final String endpoint;

    @Inject
    public RestCountriesConfig(Config config) {
        this.endpoint = config.getString("external.restcountries.endpoint");
    }

    public String getEndpoint() {
        return endpoint;
    }

}
