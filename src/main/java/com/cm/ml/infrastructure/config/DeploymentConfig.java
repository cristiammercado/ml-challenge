package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;

/**
 * Clase con configuraciones referentes al despliegue de la API en los distintos entornos.
 */
@Singleton
public class DeploymentConfig {

    private final int verticleInstances;
    private final int keepAliveTimeout;
    private final int keepAliveMax;

    @Inject
    public DeploymentConfig(Config config) {
        this.verticleInstances = config.getInt("deployment.verticle-instances");
        this.keepAliveTimeout = config.getInt("deployment.keep-alive-timeout");
        this.keepAliveMax = config.getInt("deployment.keep-alive-max");
    }

    public int getVerticleInstances() {
        return verticleInstances;
    }

    public int getKeepAliveTimeout() {
        return keepAliveTimeout;
    }

    public int getKeepAliveMax() {
        return keepAliveMax;
    }

}
