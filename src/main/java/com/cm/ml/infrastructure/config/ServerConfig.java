package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;

/**
 * Clase que contiene la configuración del servidor en donde se arrancará la API.
 */
@Singleton
public class ServerConfig {

    private final String host;
    private final int port;

    @Inject
    public ServerConfig(Config config) {
        this.host = config.getString("server.host");
        this.port = config.getInt("server.port");
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

}
