package com.cm.ml.infrastructure.config;

import com.cm.ml.infrastructure.common.EnvEnum;
import com.google.inject.Provider;
import com.google.inject.ProvisionException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Proveedor de la configuración de toda la API.
 */
public class ConfigProvider implements Provider<Config> {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigProvider.class);

    @Override
    public Config get() {
        try {
            String configKey = "application";

            if (EnvConfig.get().env() == EnvEnum.PRODUCTION) {
                configKey = "application-prod";
            }

            if (EnvConfig.get().env() == EnvEnum.TEST) {
                configKey = "application-test";
            }

            LOGGER.info("Environment config detected: " + configKey);

            return ConfigFactory.load(configKey).resolve();
        } catch (Exception e) {
            throw new ProvisionException("There was an exception provisioning config", e);
        }
    }

}
