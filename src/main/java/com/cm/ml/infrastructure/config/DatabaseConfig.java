package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;

/**
 * Clase que contiene la configuración del servidor de la bases de datos.
 */
@Singleton
public class DatabaseConfig {

    private final String host;
    private final String username;
    private final String password;
    private final long connectionTimeout;
    private final long idleTimeout;
    private final long maxLifetime;
    private final int maximumPoolSize;
    private final int minimumIdle;
    private final String driverClass;

    private final boolean autocommit;
    private final String poolName;
    private final boolean allowPoolSuspension;
    private final boolean readOnly;
    private final boolean cachedPreparedStatements;
    private final long validationTimeout;
    private final long leakDetectionThreshold;
    private final long preparedStatementCacheSize;
    private final long preparedStatementCacheSqlLimit;
    private final boolean useServerPreparedStatements;
    private final boolean useLocalTransactionState;
    private final boolean useLocalSessionState;
    private final boolean rewriteBatchedStatements;
    private final boolean cacheResultsetMetadata;
    private final boolean cacheServerConfiguration;
    private final boolean elideSetAutocommits;
    private final boolean maintainTimeStats;

    @Inject
    public DatabaseConfig(Config config) {
        this.host = config.getString("database.host");
        this.username = config.getString("database.username");
        this.password = config.getString("database.password");
        this.connectionTimeout = config.getLong("database.connection-timeout");
        this.idleTimeout = config.getLong("database.idle-timeout");
        this.maxLifetime = config.getLong("database.max-lifetime");
        this.maximumPoolSize = config.getInt("database.maximum-pool-size");
        this.minimumIdle = config.getInt("database.minimum-idle");
        this.driverClass = config.getString("database.driver-class");

        this.autocommit = true;
        this.poolName = "api.mysql";
        this.allowPoolSuspension = false;
        this.readOnly = false;
        this.cachedPreparedStatements = true;
        this.validationTimeout = 10000;
        this.leakDetectionThreshold = 30000;
        this.preparedStatementCacheSize = 500;
        this.preparedStatementCacheSqlLimit = 2048;
        this.useServerPreparedStatements = true;
        this.useLocalTransactionState = false;
        this.useLocalSessionState = true;
        this.rewriteBatchedStatements = true;
        this.cacheResultsetMetadata = true;
        this.cacheServerConfiguration = true;
        this.elideSetAutocommits = true;
        this.maintainTimeStats = false;
    }

    public String getHost() {
        return host;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getConnectionTimeout() {
        return connectionTimeout;
    }

    public long getIdleTimeout() {
        return idleTimeout;
    }

    public long getMaxLifetime() {
        return maxLifetime;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public int getMinimumIdle() {
        return minimumIdle;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public boolean isAutocommit() {
        return autocommit;
    }

    public String getPoolName() {
        return poolName;
    }

    public boolean isAllowPoolSuspension() {
        return allowPoolSuspension;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public boolean isCachedPreparedStatements() {
        return cachedPreparedStatements;
    }

    public long getValidationTimeout() {
        return validationTimeout;
    }

    public long getLeakDetectionThreshold() {
        return leakDetectionThreshold;
    }

    public long getPreparedStatementCacheSize() {
        return preparedStatementCacheSize;
    }

    public long getPreparedStatementCacheSqlLimit() {
        return preparedStatementCacheSqlLimit;
    }

    public boolean isUseServerPreparedStatements() {
        return useServerPreparedStatements;
    }

    public boolean isUseLocalTransactionState() {
        return useLocalTransactionState;
    }

    public boolean isUseLocalSessionState() {
        return useLocalSessionState;
    }

    public boolean isRewriteBatchedStatements() {
        return rewriteBatchedStatements;
    }

    public boolean isCacheResultsetMetadata() {
        return cacheResultsetMetadata;
    }

    public boolean isCacheServerConfiguration() {
        return cacheServerConfiguration;
    }

    public boolean isElideSetAutocommits() {
        return elideSetAutocommits;
    }

    public boolean isMaintainTimeStats() {
        return maintainTimeStats;
    }

}
