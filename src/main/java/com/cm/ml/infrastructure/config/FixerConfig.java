package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;

/**
 * Clase que contiene la configuración para consultar el servicio externo: Fixer.
 */
@Singleton
public class FixerConfig {

    private final String endpoint;
    private final String token;

    @Inject
    public FixerConfig(Config config) {
        this.endpoint = config.getString("external.fixer.endpoint");
        this.token = config.getString("external.fixer.token");
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getToken() {
        return token;
    }

}
