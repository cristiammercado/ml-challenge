package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;

/**
 * Clase que contiene la configuración para consultar el servicio externo: IPApi.
 */
@Singleton
public class IPApiConfig {

    private final String endpoint;
    private final String token;

    @Inject
    public IPApiConfig(Config config) {
        this.endpoint = config.getString("external.ipapi.endpoint");
        this.token = config.getString("external.ipapi.token");
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getToken() {
        return token;
    }

}
