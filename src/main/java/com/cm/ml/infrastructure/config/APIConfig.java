package com.cm.ml.infrastructure.config;

import com.google.inject.AbstractModule;
import com.typesafe.config.Config;

/**
 * Módulo de configuración de la API.
 */
public class APIConfig extends AbstractModule {

    @Override
    protected void configure() {
        bind(Config.class).toProvider(ConfigProvider.class).asEagerSingleton();
    }

}
