package com.cm.ml.infrastructure.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import io.vertx.core.http.HttpMethod;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Clase que contiene la configuración necesaria para el funcionamiento de la API a través de AJAX (Cross-origin).
 */
@Singleton
public class CORSConfig {

    private final boolean allowCredentials;
    private final String allowedOrigins;
    private final List<String> allowedHttpMetods;
    private final List<String> allowedHttpHeaders;
    private final List<String> exposedHeaders;
    private final Duration maxAge;

    @Inject
    public CORSConfig(Config config) {
        this.allowCredentials = config.getBoolean("cors.allow-credentials");
        this.allowedOrigins = config.getString("cors.allowed-origins");
        this.allowedHttpMetods = config.getStringList("cors.allowed-http-methods");
        this.allowedHttpHeaders = config.getStringList("cors.allowed-http-headers");
        this.exposedHeaders = config.getStringList("cors.exposed-headers");
        this.maxAge = config.getDuration("cors.max-age");
    }

    public boolean isAllowCredentials() {
        return allowCredentials;
    }

    public String getAllowedOrigins() {
        return allowedOrigins;
    }

    public List<HttpMethod> getAllowedHttpMethods() {
        return allowedHttpMetods
            .stream()
            .map(m -> {
                switch (m) {
                    case "POST":
                        return HttpMethod.POST;
                    case "PUT":
                        return HttpMethod.PUT;
                    case "DELETE":
                        return HttpMethod.DELETE;
                    case "OPTIONS":
                        return HttpMethod.OPTIONS;
                    case "PATCH":
                        return HttpMethod.PATCH;
                    default:
                        return HttpMethod.GET;
                }
            }).collect(Collectors.toList());
    }

    public Set<String> getAllowedHttpHeaders() {
        return new HashSet<>(allowedHttpHeaders);
    }

    public Set<String> getExposedHeaders() {
        return new HashSet<>(exposedHeaders);
    }

    public int getMaxAge() {
        return (int) maxAge.getSeconds();
    }

}
