package com.cm.ml.infrastructure.config;

import com.cm.ml.routes.RouterProvider;
import com.google.inject.AbstractModule;
import io.vertx.rxjava3.core.Vertx;
import io.vertx.rxjava3.ext.web.Router;

/**
 * Módulo principal de la API, que permite instalar los diferentes modulos del servidor de la AppVerticle.
 */
public class APIModule extends AbstractModule {

    /**
     * Instancia de Vertx.
     */
    private final Vertx vertx;

    /**
     * Constructor por parámetros de esta clase.
     *
     * @param vertx Instancia de Vertx.
     */
    public APIModule(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    protected void configure() {
        install(new APIConfig());

        bind(Vertx.class).toInstance(vertx);
        bind(Router.class).toProvider(RouterProvider.class);
    }
}
