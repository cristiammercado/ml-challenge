package com.cm.ml.infrastructure.config;

import com.cm.ml.infrastructure.common.EnvEnum;

/**
 * Clase aplicando patrón Singleton con la información del entorno actual de ejecución de la aplicación.
 */
public class EnvConfig {

    /**
     * Instancia privada de esta clase.
     */
    private static EnvConfig envConfig = null;

    /**
     * Variabla que guarda el entorno actual de ejecución.
     */
    private EnvEnum env;

    /**
     * Constructor por defecto para esta clase.
     */
    private EnvConfig() {
        this.env = EnvEnum.DEV;
    }

    /**
     * Obtiene la instancia de esta clase, si no existe se creará.
     *
     * @return Instancia de la clase EnvConfig.
     */
    public synchronized static EnvConfig get() {
        if (envConfig == null) {
            envConfig = new EnvConfig();
        }

        return envConfig;
    }

    /**
     * Obtiene el entorno actual de la aplicación.
     *
     * @return Instancia de esta clase.
     */
    public EnvEnum env() {
        return env;
    }

    /**
     * Setea globalmente el entorno de ejecución de acuerdo al valor recibido por línea de comandos.
     *
     * @param argument Valor del argumento "env".
     */
    public void set(String argument) {
        this.env = EnvEnum.DEV;

        if ("prod".equals(argument)) {
            this.env = EnvEnum.PRODUCTION;
        }

        if ("test".equals(argument)) {
            this.env = EnvEnum.TEST;
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Cannot cloned a singleton instance.");
    }

}
