package com.cm.ml.infrastructure.utils;

/**
 * Clase que contiene los parámetros actualmente aceptados por query para las listas de los diferentes módulos de la API.
 */
public class QueryParams {

    /**
     * Define la página que se obtendrá de la lista.
     */
    public static final String PAGE = "page";

    /**
     * Define la cantidad de elementos por página.
     */
    public static final String PAGE_SIZE = "pagesize";

}
