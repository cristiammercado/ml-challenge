package com.cm.ml.infrastructure.utils;

import com.cm.ml.infrastructure.exceptions.InvalidRequestBodyException;
import com.fasterxml.jackson.databind.exc.ValueInstantiationException;
import io.vertx.rxjava3.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

/**
 * Clase con métodos utilitarios.
 */
public class APIUtils {

    /**
     * Log de la aplicación.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(APIUtils.class);

    /**
     * Permite leer los parámetros de paginación.
     *
     * @param ctx Contexto del request.
     * @return Data de paginación.
     */
    public static QueryData readPagination(RoutingContext ctx) {
        Integer page = extractIntegerParam(ctx, QueryParams.PAGE);
        Integer pagesize = extractIntegerParam(ctx, QueryParams.PAGE_SIZE);

        return new QueryData(page, pagesize);
    }

    /**
     * Permite capturar un parámetro con valor textual de la URL.
     *
     * @param ctx       Routing Context.
     * @param paramName Nombre del parámetro.
     * @return Cadena leída del parámetro de la URL, o null en caso de no existir.
     */
    public static String extractStringParam(RoutingContext ctx, String paramName) {
        Optional<String> param = Optional.ofNullable(ctx.request().getParam(paramName));

        if (param.isPresent() && !param.get().isBlank()) {
            return param.get();
        }

        return null;
    }

    /**
     * Permite obtener el timestamp actual.
     *
     * @return Instancia de Timestamp.
     */
    public static Timestamp getCurrentTimestamp() {
        Date today = new Date();
        return new java.sql.Timestamp(today.getTime());
    }

    /**
     * Permite cerrar las conexiones con la base de datos.
     *
     * @param c  Instancia de la conexión.
     * @param rs Instancia del ResulSet.
     * @param ps Instancias de sentencias preparadas.
     */
    public static void closeConnection(Connection c, ResultSet rs, PreparedStatement... ps) {

        try {
            if (rs != null) {
                rs.close();
            }

            for (PreparedStatement p : ps) {
                if (p != null) {
                    p.close();
                }
            }

            if (c != null) {

                if (!c.getAutoCommit()) {
                    c.rollback();
                    c.setAutoCommit(true);
                }

                c.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Error trying to close database connection", e);
        }
    }

    /**
     * Extrae del cuerpo de la solicitud la clase recibida por parámetro.
     *
     * @param <T>   Tipo generíco configurado por parámetro.
     * @param ctx   Routing Context.
     * @param clazz Tipo de clase que se retornará.
     * @return Instancia del recurso obtenido acorde al tipo de retorno pasado por parámetro.
     */
    public static <T> T json(RoutingContext ctx, Class<T> clazz) {
        try {
            return JsonUtils.decode(ctx.getBodyAsString(), clazz);
        } catch (Exception ex) {
            if (ex instanceof ValueInstantiationException) {
                throw new InvalidRequestBodyException(ex.getMessage().split("problem: ")[1]);
            } else {
                throw new InvalidRequestBodyException();
            }
        }
    }

    /**
     * Permite capturar un parámetro con valor numérico de la URL.
     *
     * @param ctx       Routing Context.
     * @param paramName Nombre del parámetro.
     * @return Número leído del parámetro de la URL, o null en caso de no existir.
     */
    private static Integer extractIntegerParam(RoutingContext ctx, String paramName) {
        Optional<String> param = Optional.ofNullable(ctx.request().getParam(paramName));

        if (param.isPresent()) {
            try {
                return Integer.parseInt(param.get());
            } catch (NumberFormatException e) {
                LOGGER.error("Number format exception", e);
                return null;
            }
        }

        return null;
    }

}
