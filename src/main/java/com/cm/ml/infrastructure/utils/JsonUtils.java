package com.cm.ml.infrastructure.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Clase con utilidades referentes a JSON.
 */
public class JsonUtils {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);

    /**
     * Json mapper global configuration.
     */
    private static final ObjectMapper MAPPER = new ObjectMapper()
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
        .registerModule(new GuavaModule())
        .registerModule(new JodaModule())
        .registerModule(new Jdk8Module().configureAbsentsAsNulls(true))
        .setDateFormat(customDateFormat())
        .enable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    /**
     * Format for dates when parsing.
     *
     * @return Date format instance.
     */
    private static DateFormat customDateFormat() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat;
    }

    /**
     * Codifica la instancia de un objeto a formato JSON.
     *
     * @param object Instancia del objeto.
     * @return Cadena de texto en formato JSON.
     */
    public static String encode(Object object) {
        try {
            return MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to encode as JSON", e);
            return "";
        }
    }

    /**
     * @param text  Cadena en formato JSON.
     * @param clazz Tipo de clase que se retornará.
     * @param <T>   Tipo generíco configurado por parámetro.
     * @return Instancia del objeto con la información leída desde la cadena en formato JSON.
     * @throws JsonProcessingException Lanzada si ocurre un error en la decodificación.
     */
    public static <T> T decode(String text, Class<T> clazz) throws JsonProcessingException {
        return MAPPER.readValue(text, clazz);
    }

    /**
     * Decodifica una cadena de texto en formato JSON a on objeto genérico.
     *
     * @param text Cadena en formato JSON.
     * @return Instancia del objeto con la información leída desde la cadena en formato JSON.
     * @throws JsonProcessingException Lanzada si ocurre un error en la decodificación.
     */
    public static JsonNode decode(String text) throws JsonProcessingException {
        return MAPPER.readTree(text);
    }

}
