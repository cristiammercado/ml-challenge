package com.cm.ml.infrastructure.utils;

import com.cm.ml.infrastructure.common.APIConstants;

/**
 * Clase que contiene la información proveniente de la query de un endpoint.
 */
public class QueryData {

    private final Integer page;
    private final Integer pageSize;

    /**
     * Constructor por parámetros para esta clase.
     *
     * @param page     Página a obtener.
     * @param pageSize Tamaño de la página. Máximo tamaño es 100000.
     */
    public QueryData(Integer page, Integer pageSize) {

        if (page != null && page > 0) {
            this.page = page;
        } else {
            this.page = 1;
        }

        if (pageSize != null && pageSize > 0 && pageSize < 100000) {
            this.pageSize = pageSize;
        } else {
            this.pageSize = APIConstants.MAX_PAGE_SIZE;
        }
    }

    public Integer getPage() {
        return page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

}
