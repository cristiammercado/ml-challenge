package com.cm.ml.infrastructure.utils;

/**
 * Clase con métodos utilitarios para validación.
 */
public class Validators {

    /**
     * Regex para validar IPs.
     */
    private static final String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

    /**
     * Valida si el formato de la IP es correcto.
     *
     * @param ip IP a validar.
     * @return true si es válida, false en caso contrario.
     */
    public static boolean isIPValid(final String ip) {

        if (ip == null) {
            return false;
        }

        if (ip.isBlank()) {
            return false;
        }

        return ip.matches(PATTERN);
    }

}
