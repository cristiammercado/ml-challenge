package com.cm.ml.infrastructure.utils;

import javax.annotation.Nullable;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Factoría para crear los hilos en donde se ejecutarán las solicitudes HTTP.
 */
public class HttpThreadFactory implements ThreadFactory {

    /**
     * Número del pool de threads.
     */
    private static final AtomicInteger POOL_NUMBER = new AtomicInteger(1);

    /**
     * Secuencia del thread creado.
     */
    private static final AtomicInteger THREAD_NUMBER = new AtomicInteger(1);

    /**
     * Instancia del grupo de threads.
     */
    private final ThreadGroup threadGroup;

    /**
     * Prefijo de los nombres de los threads.
     */
    private final String namePrefix;

    /**
     * Constructor por defecto para esta clase.
     */
    public HttpThreadFactory() {
        SecurityManager s = System.getSecurityManager();
        this.threadGroup = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        namePrefix = "api.http-" + POOL_NUMBER.getAndIncrement() + "-thread-";
    }

    @Override
    public Thread newThread(@Nullable Runnable r) {
        Thread t = new Thread(threadGroup, r, namePrefix + THREAD_NUMBER.getAndIncrement(), 0);

        if (t.isDaemon()) {
            t.setDaemon(false);
        }

        if (t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }

        return t;
    }

}
