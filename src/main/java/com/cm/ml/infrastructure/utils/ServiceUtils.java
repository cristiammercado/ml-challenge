package com.cm.ml.infrastructure.utils;

import com.cm.ml.infrastructure.exceptions.BusinessException;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Utilidades para realizar llamadas a servicios externos.
 */
public class ServiceUtils {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceUtils.class);

    /**
     * Cliente HTTP por defecto para solicitudes a servicios externos.
     */
    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder()
        .executor(new ThreadPoolExecutor(1, 5, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(5), new HttpThreadFactory()))
        .version(HttpClient.Version.HTTP_1_1)
        .connectTimeout(Duration.of(10, SECONDS))
        .followRedirects(HttpClient.Redirect.ALWAYS)
        .build();

    /**
     * Realiza un request de tipo GET.
     *
     * @param uri URL del request.
     * @return Respuesta del servicio.
     */
    public static JsonNode makeGETRequest(URI uri) {
        LOGGER.info("Making external GET request to: " + uri.getHost());

        try {
            HttpRequest request = HttpRequest.newBuilder()
                .uri(uri)
                .timeout(Duration.of(10, SECONDS))
                .GET()
                .build();

            HttpResponse<String> response = HTTP_CLIENT.send(request, HttpResponse.BodyHandlers.ofString());
            String body = response.body();

            if (response.statusCode() != 200) {
                throw body != null ? new BusinessException(body) : new BusinessException("Error in GET request: " + response.statusCode());
            }

            return JsonUtils.decode(body);
        } catch (BusinessException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
