package com.cm.ml.infrastructure.common;

/**
 * Lista principal de constantes de la API.
 */
public class APIConstants {

    /**
     * Número máximo de elementos por página.
     */
    public static final int MAX_PAGE_SIZE = 10;

    /**
     * Llave para obtener la IP del último request.
     */
    public static final String REQUEST_IP = "api.request.ip";

}
