package com.cm.ml.infrastructure.common;

/**
 * Clase con los entornos en donde se ejecuta la API.
 */
public enum EnvEnum {

    /**
     * Entorno de producción.
     */
    PRODUCTION("Production"),

    /**
     * Entorno de desarrollo.
     */
    DEV("Development"),

    /**
     * Entorno de pruebas.
     */
    TEST("Test");

    /**
     * Nombre del ambiente.
     */
    private final String id;

    /**
     * Constructor para esta clase.
     *
     * @param id Nombre del ambiente.
     */
    EnvEnum(String id) {
        this.id = id;
    }

    /**
     * Obtiene el nombre del ambiente.
     *
     * @return Nombre del ambiente.
     */
    public String getId() {
        return id;
    }

}
