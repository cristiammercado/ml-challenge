package com.cm.ml.infrastructure.database.migrations;

import com.cm.ml.infrastructure.config.DatabaseConfig;
import com.google.inject.Inject;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.jdbc.DriverDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase que ejecuta las migraciones de la base de datos de la AppVerticle.
 */
public class Migrator {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Migrator.class);

    /**
     * Prefijo de los archivos de las migraciones de base de datos.
     */
    private final String prefix;

    /**
     * Separador de la versión con el nombre.
     */
    private final String separator;

    /**
     * Sufijo de los archivos de las migraciones de base de datos.
     */
    private final String suffix;

    /**
     * Nombre de la tabla de control de migraciones.
     */
    private final String table;

    /**
     * Configuración de la base de datos.
     */
    private final DatabaseConfig config;

    /**
     * Constructor por parámetro para esta clase.
     *
     * @param config Instancia con datos de configuración de la base de datos.
     */
    @Inject
    public Migrator(DatabaseConfig config) {
        this.prefix = "v";
        this.separator = "__";
        this.suffix = ".sql";
        this.table = "db_migrations";
        this.config = config;
    }

    /**
     * Inicia la migración de las bases de datos.
     */
    public void execute() {
        LOGGER.info("Starting database migrations...");

        DriverDataSource dds = new DriverDataSource(Thread.currentThread().getContextClassLoader(),
            this.config.getDriverClass(),
            this.config.getHost(),
            this.config.getUsername(),
            this.config.getPassword()
        );

        Flyway flyway = Flyway.configure()
            .baselineOnMigrate(true)
            .dataSource(dds)
            .encoding("UTF-8")
            .installedBy("API_MIGRATOR")
            .locations("migrations")
            .sqlMigrationPrefix(prefix)
            .sqlMigrationSeparator(separator)
            .sqlMigrationSuffixes(suffix)
            .table(table)
            .load();

        flyway.repair();
        flyway.migrate();

        LOGGER.info("End database migrations");
    }

}
