package com.cm.ml.infrastructure.database;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Clase aplicando Singleton de la conexión al servidor de la base de datos.
 */
@Singleton
public class DatabaseConnection {

    /**
     * Pool de conexiones a la base de datos.
     */
    private final ConnectionPool connectionPool;

    /**
     * Constructor por defecto para esta clase.
     *
     * @param connectionPool Instancia que administra el pool de conexiones con la base de datos.
     */
    @Inject
    public DatabaseConnection(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
        this.connectionPool.start();
    }

    /**
     * Obtener una conexión del pool de conexiones.
     *
     * @return Instancia de la conexión.
     * @throws SQLException Es lanzada si ocurre un error al obtener la conexión con el servidor MySQL.
     */
    public Connection get() throws SQLException {
        return this.connectionPool.getDataSource().getConnection();
    }

}
