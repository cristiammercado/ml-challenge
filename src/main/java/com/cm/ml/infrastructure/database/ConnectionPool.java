package com.cm.ml.infrastructure.database;

import com.cm.ml.infrastructure.config.DatabaseConfig;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase que asocia los pool de conexiones a las bases de datos de producción y de pruebas.
 */
@Singleton
public class ConnectionPool {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionPool.class);

    /**
     * Configuración del cliente de la base de datos.
     */
    private final HikariConfig config;

    /**
     * Datasource de conexión para la base de datos MySQL.
     */
    private HikariDataSource dataSource;

    /**
     * Constructor por defecto para esta clase.
     *
     * @param databaseConfig Parámetros de configuración de la base de datos.
     */
    @Inject
    public ConnectionPool(DatabaseConfig databaseConfig) {
        this.config = new HikariConfig();

        this.config.setJdbcUrl(databaseConfig.getHost());

        this.config.addDataSourceProperty("cachePrepStmts", databaseConfig.isCachedPreparedStatements());
        this.config.addDataSourceProperty("prepStmtCacheSize", databaseConfig.getPreparedStatementCacheSize());
        this.config.addDataSourceProperty("prepStmtCacheSqlLimit", databaseConfig.getPreparedStatementCacheSqlLimit());
        this.config.addDataSourceProperty("useServerPrepStmts", databaseConfig.isUseServerPreparedStatements());
        this.config.addDataSourceProperty("useLocalTransactionState", databaseConfig.isUseLocalTransactionState());
        this.config.addDataSourceProperty("useLocalSessionState", databaseConfig.isUseLocalSessionState());
        this.config.addDataSourceProperty("rewriteBatchedStatements", databaseConfig.isRewriteBatchedStatements());
        this.config.addDataSourceProperty("cacheResultSetMetadata", databaseConfig.isCacheResultsetMetadata());
        this.config.addDataSourceProperty("cacheServerConfiguration", databaseConfig.isCacheServerConfiguration());
        this.config.addDataSourceProperty("elideSetAutoCommits", databaseConfig.isElideSetAutocommits());
        this.config.addDataSourceProperty("maintainTimeStats", databaseConfig.isMaintainTimeStats());

        this.config.setUsername(databaseConfig.getUsername());
        this.config.setPassword(databaseConfig.getPassword());
        this.config.setAutoCommit(databaseConfig.isAutocommit());
        this.config.setConnectionTimeout(databaseConfig.getConnectionTimeout());
        this.config.setIdleTimeout(databaseConfig.getIdleTimeout());
        this.config.setMaxLifetime(databaseConfig.getMaxLifetime());
        this.config.setMaximumPoolSize(databaseConfig.getMaximumPoolSize());
        this.config.setMinimumIdle(databaseConfig.getMinimumIdle());
        this.config.setPoolName(databaseConfig.getPoolName());
        this.config.setAllowPoolSuspension(databaseConfig.isAllowPoolSuspension());
        this.config.setReadOnly(databaseConfig.isReadOnly());
        this.config.setDriverClassName(databaseConfig.getDriverClass());
        this.config.setValidationTimeout(databaseConfig.getValidationTimeout());
        this.config.setLeakDetectionThreshold(databaseConfig.getLeakDetectionThreshold());
    }

    /**
     * Inicia el cliente JDBC.
     */
    public void start() {
        this.dataSource = new HikariDataSource(config);
        LOGGER.info("Create ConnectionPool instance");
    }

    /**
     * Obtiene el datasource creado.
     *
     * @return Instancia de HikariDataSource.
     */
    HikariDataSource getDataSource() {
        return dataSource;
    }

}
