package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: RestCountries no retornó la información esperada.
 */
public class RestCountriesException extends BusinessException {

    /**
     * Constructor por defecto.
     */
    public RestCountriesException() {
        super("Response from RestCountries without complete information or null", ErrorCode.EXTERNAL_API_ERROR, Status.SERVICE_UNAVAILABLE);
    }

    /**
     * Constructor cuando hay un error diferente al genérico.
     *
     * @param message Información del error.
     */
    public RestCountriesException(String message) {
        super("Response from RestCountries without complete information or null: " + message, ErrorCode.MISSED_INFORMATION);
    }

}
