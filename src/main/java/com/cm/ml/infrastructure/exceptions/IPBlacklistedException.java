package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: IP se encuentra en la blacklist.
 */
public class IPBlacklistedException extends BusinessException {

    /**
     * Constructor por defecto.
     */
    public IPBlacklistedException(String ip) {
        super("IP is blacklisted: " + ip, ErrorCode.IP_BLACKLISTED, Status.FORBIDDEN);
    }

}
