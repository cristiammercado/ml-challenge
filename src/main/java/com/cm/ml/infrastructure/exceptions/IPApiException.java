package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: IP Api no retornó la información esperada.
 */
public class IPApiException extends BusinessException {

    /**
     * Constructor por defecto.
     */
    public IPApiException() {
        super("Response from IPApi without complete information or null", ErrorCode.EXTERNAL_API_ERROR, Status.SERVICE_UNAVAILABLE);
    }

    /**
     * Constructor cuando hay un error diferente al genérico.
     *
     * @param message Información del error.
     */
    public IPApiException(String message) {
        super("Response from IPApi without complete information or null: " + message, ErrorCode.MISSED_INFORMATION);
    }

}
