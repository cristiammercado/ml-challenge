package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: Conflicto al crear el recurso.
 */
public class CreateConflictException extends BusinessException {

    /**
     * Constructor por parámetros para esta clase.
     *
     * @param message Mensaje de la excepción original.
     */
    public CreateConflictException(String message) {
        super(message, ErrorCode.CREATE_RESOURCE_CONFLICT, Status.CONFLICT);
    }

}
