package com.cm.ml.infrastructure.exceptions;


import com.cm.ml.routes.Status;

/**
 * Excepción: Conflicto al actualizar/eliminar el recurso.
 */
public class UpdateConflictException extends BusinessException {

    /**
     * Constructor por parámetros para esta clase.
     *
     * @param message Mensaje de la excepción original.
     */
    public UpdateConflictException(String message) {
        super(message, ErrorCode.UPDATE_RESOURCE_CONFLICT, Status.CONFLICT);
    }

}
