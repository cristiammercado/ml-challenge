package com.cm.ml.infrastructure.exceptions;

/**
 * Códigos de error de negocio generados en la API.
 */
public class ErrorCode {

    /**
     * Error genérico, no usar.
     */
    public static final int GENERIC_ERROR = 1;

    /**
     * Parámetro URL inválido.
     */
    static final int INVALID_URL_PARAM = 2;

    /**
     * Cuerpo de la solicitud inválido.
     */
    static final int INVALID_REQUEST_BODY = 4;

    /**
     * Conflicto al crear el recurso.
     */
    public static final int CREATE_RESOURCE_CONFLICT = 6;

    /**
     * URL o recurso no encontrado.
     */
    public static final int URL_OR_RESOURCE_NOT_FOUND = 7;

    /**
     * Conflicto al actualizar el recurso.
     */
    public static final int UPDATE_RESOURCE_CONFLICT = 8;

    /**
     * Respuesta sin información completa.
     */
    public static final int MISSED_INFORMATION = 9;

    /**
     * Error recibido del servicio externo.
     */
    public static final int EXTERNAL_API_ERROR = 10;

    /**
     * IP existe en blacklist.
     */
    public static final int IP_BLACKLISTED = 11;

}
