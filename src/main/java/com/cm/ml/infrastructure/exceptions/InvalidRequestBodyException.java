package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: Cuerpo de solicitud inválido.
 */
public class InvalidRequestBodyException extends BusinessException {

    /**
     * Constructor.
     */
    public InvalidRequestBodyException() {
        super("The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed", ErrorCode.INVALID_REQUEST_BODY, Status.UNPROCCESSABLE_ENTITY);
    }

    /**
     * Constructor.
     *
     * @param message Mensaje con errores de validación.
     */
    public InvalidRequestBodyException(String message) {
        super(message, ErrorCode.INVALID_REQUEST_BODY, Status.UNPROCCESSABLE_ENTITY);
    }

}
