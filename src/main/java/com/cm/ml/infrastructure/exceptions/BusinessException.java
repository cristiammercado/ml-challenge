package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: Clase padre de las excepciones de la API.
 */
public class BusinessException extends RuntimeException {

    /**
     * Número del error acorde a la clase ErrorCode.
     */
    private final int errorCode;

    /**
     * Número del error acorde a los errores HTTP.
     */
    private final int statusCode;

    /**
     * Constructor por defecto.
     */
    public BusinessException() {
        this("Error in API", ErrorCode.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR);
    }

    /**
     * Constructor con un mensaje en específico.
     */
    public BusinessException(String message) {
        this(message, ErrorCode.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR);
    }

    /**
     * Constructor con un mensaje y código en específico.
     *
     * @param message   Mensaje de error.
     * @param errorCode Número de error.
     */
    public BusinessException(String message, int errorCode) {
        this(message, errorCode, Status.INTERNAL_SERVER_ERROR);
    }

    /**
     * Constructor con un mensaje del usuario, número de error del usuario y un código de error ajustados a los del protocolo HTTP.
     *
     * @param message   Mensaje de error.
     * @param errorCode Número de error.
     * @param status    Código de estado HTTP.
     */
    public BusinessException(String message, int errorCode, int status) {
        super(message);
        this.errorCode = errorCode;
        this.statusCode = status;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    int getErrorCode() {
        return errorCode;
    }

}
