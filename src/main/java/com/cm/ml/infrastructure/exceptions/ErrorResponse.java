package com.cm.ml.infrastructure.exceptions;

/**
 * Clase para las respuestas de error de la API.
 */
public class ErrorResponse {

    /**
     * Código de error.
     */
    private final int code;

    /**
     * Mensaje de error.
     */
    private final String message;

    /**
     * Constructor por parámetros para esta clase.
     *
     * @param code    Código del error.
     * @param message Mensaje de error.
     */
    public ErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * Constructor por parámetros para esta clase.
     *
     * @param ae Excepción extendida de ApiRuntimeException.
     */
    public ErrorResponse(BusinessException ae) {
        this.code = ae.getErrorCode();
        this.message = ae.getMessage();
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
