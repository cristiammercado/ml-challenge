package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: Fixer no retornó la información esperada.
 */
public class FixerException extends BusinessException {

    /**
     * Constructor por defecto.
     */
    public FixerException() {
        super("Response from Fixer without complete information or null", ErrorCode.EXTERNAL_API_ERROR, Status.SERVICE_UNAVAILABLE);
    }

    /**
     * Constructor cuando hay un error diferente al genérico.
     *
     * @param message Información del error.
     */
    public FixerException(String message) {
        super("Response from Fixer without complete information or null: " + message, ErrorCode.MISSED_INFORMATION);
    }

}
