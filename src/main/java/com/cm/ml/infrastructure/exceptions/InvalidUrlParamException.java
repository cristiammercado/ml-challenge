package com.cm.ml.infrastructure.exceptions;

import com.cm.ml.routes.Status;

/**
 * Excepción: Parámetro URL inválido.
 */
public class InvalidUrlParamException extends BusinessException {

    /**
     * Constructor por parámetro para esta clase.
     *
     * @param parameter Parámetro érroneo.
     */
    public InvalidUrlParamException(String parameter) {
        super("Invalid URL parameter: " + parameter, ErrorCode.INVALID_URL_PARAM, Status.BAD_REQUEST);
    }

}
