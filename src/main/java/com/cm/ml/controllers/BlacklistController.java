package com.cm.ml.controllers;

import com.cm.ml.dto.BlacklistDTO;
import com.cm.ml.dto.ListDTO;
import com.cm.ml.infrastructure.exceptions.InvalidUrlParamException;
import com.cm.ml.infrastructure.utils.APIUtils;
import com.cm.ml.infrastructure.utils.QueryData;
import com.cm.ml.infrastructure.utils.Validators;
import com.cm.ml.services.BlacklistService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.vertx.core.Future;
import io.vertx.rxjava3.ext.web.RoutingContext;

/**
 * Controlador para rutas de estado de la API.
 */
@Singleton
public class BlacklistController extends Controller {

    /**
     * Servicio con lógica de negocio asociada a este controlador.
     */
    private final BlacklistService blacklistService;

    @Inject
    public BlacklistController(BlacklistService blacklistService) {
        this.blacklistService = blacklistService;
    }

    public void list(RoutingContext ctx) {
        Future.future(p -> {
            QueryData qd = APIUtils.readPagination(ctx);
            p.complete(blacklistService.listByPage(qd));
        }).onSuccess(r -> buildGetResponse(ctx, r)).onFailure(error -> handleFailure(ctx, error));
    }

    public void post(RoutingContext ctx) {
        Future.future(p -> {
            BlacklistDTO dto = APIUtils.json(ctx, BlacklistDTO.class);
            blacklistService.add(dto);
            p.complete();
        }).onSuccess(r -> buildPostResponse(ctx)).onFailure(error -> handleFailure(ctx, error));
    }

    public void delete(RoutingContext ctx) {
        Future.future(p -> {
            String ip = APIUtils.extractStringParam(ctx, "ip");
            boolean isValid = Validators.isIPValid(ip);

            if (!isValid) {
                p.fail(new InvalidUrlParamException("ip"));
                return;
            }

            blacklistService.delete(ip);
            p.complete();
        }).onSuccess(r -> buildUpdateResponse(ctx)).onFailure(error -> handleFailure(ctx, error));
    }

}
