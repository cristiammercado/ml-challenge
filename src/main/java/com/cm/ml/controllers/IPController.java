package com.cm.ml.controllers;

import com.cm.ml.infrastructure.common.APIConstants;
import com.cm.ml.infrastructure.exceptions.InvalidUrlParamException;
import com.cm.ml.infrastructure.utils.APIUtils;
import com.cm.ml.infrastructure.utils.Validators;
import com.cm.ml.services.IPService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.vertx.core.Future;
import io.vertx.rxjava3.core.Vertx;
import io.vertx.rxjava3.ext.web.RoutingContext;

/**
 * Controlador para rutas de estado de la API.
 */
@Singleton
public class IPController extends Controller {

    private final IPService ipService;

    private final Vertx vertx;

    @Inject
    public IPController(IPService ipService, Vertx vertx) {
        this.ipService = ipService;
        this.vertx = vertx;
    }

    public void getInfoFromRequest(RoutingContext ctx) {
        Future.future(p -> {
            String ip = ctx.get(APIConstants.REQUEST_IP);
            p.complete(ipService.ipInfo(ip));
        }).onSuccess(r -> buildGetResponse(ctx, r)).onFailure(error -> handleFailure(ctx, error));
    }

    public void getInfoFromURL(RoutingContext ctx) {
        this.vertx.rxExecuteBlocking(evt -> Future.future(p -> {
            String ip = APIUtils.extractStringParam(ctx, "ip");
            boolean isValid = Validators.isIPValid(ip);

            if (!isValid) {
                p.fail(new InvalidUrlParamException("ip"));
                evt.complete();
                return;
            }

            p.complete(ipService.ipInfo(ip));
            evt.complete();
        }).onSuccess(r -> buildGetResponse(ctx, r)).onFailure(error -> handleFailure(ctx, error))).subscribe();
    }

}
