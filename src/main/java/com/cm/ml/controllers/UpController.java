package com.cm.ml.controllers;

import com.cm.ml.services.UpService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.vertx.core.Future;
import io.vertx.rxjava3.ext.web.RoutingContext;

/**
 * Controlador para rutas de estado de la API.
 */
@Singleton
public class UpController extends Controller {

    private final UpService upService;

    @Inject
    public UpController(UpService upService) {
        this.upService = upService;
    }

    public void status(RoutingContext ctx) {
        Future.future(p -> p.complete(upService.status()))
            .onSuccess(r -> buildGetResponse(ctx, r)).onFailure(error -> handleFailure(ctx, error));
    }

}
