package com.cm.ml.controllers;

import com.cm.ml.dto.APIResponseDTO;
import com.cm.ml.infrastructure.exceptions.BusinessException;
import com.cm.ml.infrastructure.exceptions.ErrorCode;
import com.cm.ml.infrastructure.exceptions.ErrorResponse;
import com.cm.ml.infrastructure.utils.JsonUtils;
import com.cm.ml.routes.Status;
import io.vertx.rxjava3.ext.web.RoutingContext;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * Implementa acciones comunes en los controladores.
 */
public class Controller {

    /**
     * Respuesta: Obtención de un recurso por su identificador.
     *
     * @param ctx  Routing Context.
     * @param data Información del recurso.
     */
    protected void buildGetResponse(RoutingContext ctx, Object data) {
        this.buildResponse(ctx, Status.OK, data, null);
    }

    /**
     * Respuesta: Creación de un recurso.
     *
     * @param ctx Routing Context.
     */
    protected void buildPostResponse(RoutingContext ctx) {
        this.buildResponse(ctx, Status.CREATED, null, null);
    }

    /**
     * Respuesta: Modificación/Eliminación de un recurso.
     *
     * @param ctx Routing Context.
     */
    protected void buildUpdateResponse(RoutingContext ctx) {
        this.buildResponse(ctx, Status.OK, null, null);
    }

    /**
     * Respuesta: Excepción extendida de ApiException.
     *
     * @param ctx       Routing Context.
     * @param exception Excepción extendida de ApiException.
     */
    protected void buildExceptionResponse(RoutingContext ctx, BusinessException exception) {
        ErrorResponse errorResponse = new ErrorResponse(exception);
        buildResponse(ctx, exception.getStatusCode(), null, errorResponse);
    }

    /**
     * Respuesta: Excepción NO extendida de ApiException.
     *
     * @param ctx       Routing Context.
     * @param throwable Excepción.
     */
    protected void buildExceptionResponse(RoutingContext ctx, Throwable throwable) {
        ErrorResponse errorResponse = new ErrorResponse(ErrorCode.GENERIC_ERROR, throwable.getMessage());
        buildResponse(ctx, Status.INTERNAL_SERVER_ERROR, null, errorResponse);
    }

    /**
     * Responde una petición http con un JSON
     *
     * @param ctx           Routing Context.
     * @param statusCode    Código de estado HTTP de la respuesta.
     * @param data          Objeto con la información de respuesta correcta.
     * @param errorResponse Objeto con la información de la respuesta érronea.
     */
    protected void buildResponse(RoutingContext ctx, int statusCode, Object data, ErrorResponse errorResponse) {

        APIResponseDTO response = APIResponseDTO.builder()
            .data(Optional.ofNullable(data))
            .error(Optional.ofNullable(errorResponse))
            .success(statusCode >= 200 && statusCode < 400)
            .build();

        ctx.response()
            .setStatusCode(statusCode)
            .end(JsonUtils.encode(response), StandardCharsets.UTF_8.name());
    }

    /**
     * Captura el error generado dentro de un comando de Hystrix. Valida si es
     * del negocio o si es un error independiente.
     *
     * @param ctx       Routing Context.
     * @param throwable Exceptión generada.
     */
    protected void handleFailure(RoutingContext ctx, Throwable throwable) {
        if (throwable instanceof BusinessException) {
            buildExceptionResponse(ctx, (BusinessException) throwable);
        } else {
            buildExceptionResponse(ctx, throwable);
        }
    }

}
