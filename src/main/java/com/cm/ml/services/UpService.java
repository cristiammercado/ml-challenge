package com.cm.ml.services;

import com.cm.ml.dto.APIStatusDTO;

/**
 * Servicio para la lógica de negocio del estado de la API.
 */
public class UpService {

    public APIStatusDTO status() {
        return APIStatusDTO.builder().build();
    }

}
