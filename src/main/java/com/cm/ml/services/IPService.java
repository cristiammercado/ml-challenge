package com.cm.ml.services;

import com.cm.ml.acl.fixer.FixerService;
import com.cm.ml.acl.fixer.domain.ExchangeRates;
import com.cm.ml.acl.ipapi.IPApiService;
import com.cm.ml.acl.ipapi.domain.CountryInfo;
import com.cm.ml.acl.restcountries.RestCountriesService;
import com.cm.ml.acl.restcountries.domain.CurrencyInfo;
import com.cm.ml.dao.BlacklistDAO;
import com.cm.ml.domain.IPInfo;
import com.cm.ml.infrastructure.exceptions.IPBlacklistedException;
import com.google.inject.Inject;

/**
 * Servicio para la lógica de negocio de consulta de información de una IP.
 */
public class IPService {

    private final BlacklistDAO blacklistDAO;

    private final IPApiService ipApiService;

    private final RestCountriesService restCountriesService;

    private final FixerService fixerService;

    @Inject
    public IPService(BlacklistDAO blacklistDAO, IPApiService ipApiService, RestCountriesService restCountriesService, FixerService fixerService) {
        this.blacklistDAO = blacklistDAO;
        this.ipApiService = ipApiService;
        this.restCountriesService = restCountriesService;
        this.fixerService = fixerService;
    }

    public IPInfo ipInfo(String ip) {
        boolean ipBlacklisted = this.blacklistDAO.isIPBlacklisted(ip);

        if (ipBlacklisted) {
            throw new IPBlacklistedException(ip);
        }

        CountryInfo countryInfo = this.ipApiService.getIPCountry(ip);
        CurrencyInfo currencyInfo = this.restCountriesService.getCountryCurrency(countryInfo.countryCode());
        ExchangeRates exchangeRates = this.fixerService.getExchangeData(currencyInfo.currency());

        return IPInfo.builder()
            .ip(ip)
            .country(countryInfo.countryName())
            .isoCode(countryInfo.countryCode())
            .currency(currencyInfo.currency())
            .usdExchangeRate(exchangeRates.usd())
            .eurExchangeRate(exchangeRates.eur())
            .build();
    }

}
