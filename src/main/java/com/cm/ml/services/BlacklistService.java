package com.cm.ml.services;

import com.cm.ml.dao.BlacklistDAO;
import com.cm.ml.domain.Blacklist;
import com.cm.ml.dto.BlacklistDTO;
import com.cm.ml.dto.ListDTO;
import com.cm.ml.infrastructure.utils.QueryData;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.List;

/**
 * Servicio para la lógica de negocio del estado de la API.
 */
@Singleton
public class BlacklistService {

    private final BlacklistDAO blacklistDAO;

    @Inject
    public BlacklistService(BlacklistDAO blacklistDAO) {
        this.blacklistDAO = blacklistDAO;
    }

    public ListDTO listByPage(QueryData qd) {
        List<Blacklist> list = this.blacklistDAO.listByPage(qd);

        return ListDTO.builder()
            .records(list.size())
            .list(list)
            .build();
    }

    public void add(BlacklistDTO dto) {
        this.blacklistDAO.create(dto.ip());
    }

    public void delete(String ip) {
        this.blacklistDAO.delete(ip);
    }

}
