package com.cm.ml;

import com.cm.ml.infrastructure.config.APIModule;
import com.cm.ml.infrastructure.config.DeploymentConfig;
import com.cm.ml.infrastructure.config.EnvConfig;
import com.cm.ml.infrastructure.config.ServerConfig;
import com.cm.ml.infrastructure.database.migrations.Migrator;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.rxjava3.core.AbstractVerticle;
import io.vertx.rxjava3.core.http.HttpServer;
import io.vertx.rxjava3.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Clase que arranca el servidor principal de la API.
 */
public class MainVerticle extends AbstractVerticle {

    /**
     * Logger de esta clase.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

    /**
     * Instancia con lógica para migración de base de datos.
     */
    @Inject
    private Migrator migrator;

    /**
     * Ruteador principal de la aplicación.
     */
    @Inject
    private Router router;

    /**
     * Configuración de servidor.
     */
    @Inject
    private ServerConfig serverConfig;

    /**
     * Configuración de despliegue.
     */
    @Inject
    private DeploymentConfig deploymentConfig;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

        Injector injector = Guice.createInjector(new APIModule(this.vertx));
        injector.injectMembers(this);
    }

    @Override
    public void start() {

        // Ejecuta migraciones de base de datos
        this.migrator.execute();

        // Configuramos opciones de servidor HTTP
        HttpServerOptions httpServerOptions = new HttpServerOptions()
            .setCompressionSupported(true)
            .setCompressionLevel(6)
            .setTcpKeepAlive(true)
            .setIdleTimeout(this.deploymentConfig.getKeepAliveTimeout())
            .setIdleTimeoutUnit(TimeUnit.SECONDS);

        // Inyectamos rutas de API
        HttpServer httpServer = vertx.createHttpServer(httpServerOptions);
        Disposable d = httpServer.requestStream().toObservable().subscribe(router::handle);

        // Iniciamos servidor HTTP
        Disposable d2 = httpServer.listen(this.serverConfig.getPort(), this.serverConfig.getHost())
            .subscribe(
                server -> this.printServerInfo(),
                err -> {
                    LOGGER.error("Error starting HTTP server on " + this.serverConfig.getHost() + ":" + this.serverConfig.getPort(), err);
                    LOGGER.warn("Finishing application...");
                    d.dispose();
                    System.exit(-1);
                }
            );

        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(d2);
    }

    /**
     * Imprime la información de inicio en el log.
     */
    private void printServerInfo() {

        String s = "\n\n\t\t" +
            "================= ML CHALLENGE API ==============\n\t\t" +
            "===== Environment: " + EnvConfig.get().env().getId() + "\n\t\t" +
            "===== HTTP Server: " + this.serverConfig.getHost() + ":" + this.serverConfig.getPort() + "\n\t\t" +
            "===== Time info:   " + new Date() + "\n\t\t" +
            "=================================================\n";

        LOGGER.info(s);
    }

}
