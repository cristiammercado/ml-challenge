SET NAMES utf8mb4;

CREATE TABLE blacklist
(
    ip         varchar(15) NOT NULL,
    created_at timestamp   NOT NULL,
    PRIMARY KEY (ip)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
